﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class ServicoService : IServicoService
    {
        private readonly IBaseRepository<Servico> _repository;

        private readonly IServicoRepository _servicoRepository;

        private readonly IServicoConverter _servicoConverter;

        private readonly IResponseServicoConverter _responseServicoConverter;

        private StatusValidator _statusValidator = new StatusValidator();

        public ServicoService(IBaseRepository<Servico> repository, IServicoRepository servicoRepository, IServicoConverter servicoConverter, IResponseServicoConverter responseServicoConverter)
        {
            _repository = repository;
            _servicoRepository = servicoRepository;
            _servicoConverter = servicoConverter;
            _responseServicoConverter = responseServicoConverter;
        }

        public void Post<ServicoDTOValidator>(ServicoDTO servicoDTO) where ServicoDTOValidator : AbstractValidator<ServicoDTO>
        {
            if (servicoDTO.Id == ConstantesGerais.ZERO)
            {
                Validate(servicoDTO, Activator.CreateInstance<ServicoDTOValidator>());

                var servico = _servicoConverter.Conversion(servicoDTO);

                servico.DataCadastro = DateTime.Now;

                servico.DataUltimaAlteracao = DateTime.Now;

                _repository.Insert(servico);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSer_0.GetMessage("código do serviço"));
            }
        }

        public void Put<ServicoDTOValidator>(int id, ServicoDTO servicoDTO) where ServicoDTOValidator : AbstractValidator<ServicoDTO>
        {
            var servico = _repository.SelectById(id);

            if (servicoDTO.Id > ConstantesGerais.ZERO && servicoDTO.Id == servico.Id)
            {
                Validate(servicoDTO, Activator.CreateInstance<ServicoDTOValidator>());

                servico.Descricao = servicoDTO.Descricao;
                servico.Preco = servicoDTO.Preco;

                servico.DataCadastro = GetRegistrationDateById(servico.Id);

                servico.DataUltimaAlteracao = DateTime.Now;

                _repository.Update(servico);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do serviço"));
            }
        }

        public void Patch(int id, bool status)
        {
            var servico = _repository.SelectById(id);

            if (servico == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Serviço"));

            _statusValidator.ValidateStatus(status);

            servico.Status = status;

            servico.DataUltimaAlteracao = DateTime.Now;

            _repository.Update(servico);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do serviço"));
            }
        }

        public ServicoDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new ServicoDTO();

                var servico = _repository.SelectById(id);

                ServicoDTO servicoDTO = _servicoConverter.Conversion(servico);

                return servicoDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do serviço"));
            }
        }

        public IList<ServicoDTO> GetAll()
        {
            IList<ServicoDTO> listaServicosDTO;

            var servicos = _repository.SelectAll();

            listaServicosDTO = _servicoConverter.ListConversion(servicos);

            return listaServicosDTO;
        }

        public ICollection<ResponseServicoDTO> GetAll(int pageIndex, int pageSize, ServicoFilter servicoFilter)
        {
            ICollection<ResponseServicoDTO> listaResponseServicosDTO;

            var servicos = _servicoRepository.SelectAll(pageIndex, pageSize, servicoFilter);

            listaResponseServicosDTO = _responseServicoConverter.CollectionConversion(servicos);

            return listaResponseServicosDTO;
        }

        private void Validate(ServicoDTO servicoDTO, AbstractValidator<ServicoDTO> validator)
        {
            if (servicoDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Serviço"));

            validator.ValidateAndThrow(servicoDTO);
        }

        private DateTime GetRegistrationDateById(int id)
        {
            return _servicoRepository.SelectRegistrationDateById(id);
        }
    }
}