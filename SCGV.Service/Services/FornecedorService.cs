﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class FornecedorService : IFornecedorService
    {
        private readonly IBaseRepository<Fornecedor> _repository;

        private readonly IFornecedorRepository _fornecedorRepository;

        private readonly IFornecedorConverter _fornecedorConverter;

        private readonly IResponseFornecedorConverter _responseFornecedorConverter;

        private StatusValidator _statusValidator = new StatusValidator();

        public FornecedorService(IBaseRepository<Fornecedor> repository, IFornecedorRepository fornecedorRepository, IFornecedorConverter fornecedorConverter, IResponseFornecedorConverter responseFornecedorConverter)
        {
            _repository = repository;
            _fornecedorRepository = fornecedorRepository;
            _fornecedorConverter = fornecedorConverter;
            _responseFornecedorConverter = responseFornecedorConverter;
        }

        public void Post<FornecedorDTOValidator>(FornecedorDTO fornecedorDTO) where FornecedorDTOValidator : AbstractValidator<FornecedorDTO>
        {
            if (fornecedorDTO.Id == ConstantesGerais.ZERO)
            {
                Validate(fornecedorDTO, Activator.CreateInstance<FornecedorDTOValidator>());

                var fornecedor = _fornecedorConverter.Conversion(fornecedorDTO);

                fornecedor.DataCadastro = DateTime.Now;

                fornecedor.DataUltimaAlteracao = DateTime.Now;

                _repository.Insert(fornecedor);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSer_0.GetMessage("matrícula do fornecedor"));
            }
        }

        public void Put<FornecedorDTOValidator>(int id, FornecedorDTO fornecedorDTO) where FornecedorDTOValidator : AbstractValidator<FornecedorDTO>
        {
            var fornecedor = _repository.SelectById(id);

            if (fornecedorDTO.Id > ConstantesGerais.ZERO && fornecedorDTO.Id == fornecedor.Id)
            {
                Validate(fornecedorDTO, Activator.CreateInstance<FornecedorDTOValidator>());

                fornecedor.RazaoSocial = fornecedorDTO.RazaoSocial;
                fornecedor.NomeFantasia = fornecedorDTO.NomeFantasia;
                fornecedor.CNPJ = fornecedorDTO.CNPJ;
                fornecedor.NumeroTelefoneCelular = fornecedorDTO.NumeroTelefoneCelular;

                fornecedor.DataCadastro = GetRegistrationDateById(fornecedor.Id);

                fornecedor.DataUltimaAlteracao = DateTime.Now;

                _repository.Update(fornecedor);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do fornecedor"));
            }
        }

        public void Patch(int id, bool status)
        {
            var fornecedor = _repository.SelectById(id);

            if (fornecedor == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Fornecedor"));

            _statusValidator.ValidateStatus(status);

            fornecedor.Status = status;

            fornecedor.DataUltimaAlteracao = DateTime.Now;

            _repository.Update(fornecedor);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do cliente"));
            }
        }

        public FornecedorDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new FornecedorDTO();

                var fornecedor = _repository.SelectById(id);

                FornecedorDTO fornecedorDTO = _fornecedorConverter.Conversion(fornecedor);

                return fornecedorDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do fornecedor"));
            }
        }

        public IList<FornecedorDTO> GetAll()
        {
            IList<FornecedorDTO> listaFornecedoresDTO;

            var fornecedores = _repository.SelectAll();

            listaFornecedoresDTO = _fornecedorConverter.ListConversion(fornecedores);

            return listaFornecedoresDTO;
        }

        public ICollection<ResponseFornecedorDTO> GetAll(int pageIndex, int pageSize, FornecedorFilter fornecedorFilter)
        {
            ICollection<ResponseFornecedorDTO> listaResponseFornecedoresDTO;

            var fornecedores = _fornecedorRepository.SelectAll(pageIndex, pageSize, fornecedorFilter);

            listaResponseFornecedoresDTO = _responseFornecedorConverter.CollectionConversion(fornecedores);

            return listaResponseFornecedoresDTO;
        }

        private void Validate(FornecedorDTO fornecedorDTO, AbstractValidator<FornecedorDTO> validator)
        {
            if (fornecedorDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Fornecedor"));

            validator.ValidateAndThrow(fornecedorDTO);
        }

        private DateTime GetRegistrationDateById(int id)
        {
            return _fornecedorRepository.SelectRegistrationDateById(id);
        }
    }
}