﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class ClienteService : IClienteService
    {
        private readonly IBaseRepository<Cliente> _repository;

        private readonly IClienteRepository _clienteRepository;

        private readonly IClienteConverter _clienteConverter;

        private readonly IResponseClienteConverter _responseClienteConverter;

        private StatusValidator _statusValidator = new StatusValidator();

        public ClienteService(IBaseRepository<Cliente> repository, IClienteRepository clienteRepository, IClienteConverter clienteConverter, IResponseClienteConverter responseClienteConverter)
        {
            _repository = repository;
            _clienteRepository = clienteRepository;
            _clienteConverter = clienteConverter;
            _responseClienteConverter = responseClienteConverter;
        }

        public void Post<ClienteDTOValidator>(ClienteDTO clienteDTO) where ClienteDTOValidator : AbstractValidator<ClienteDTO>
        {
            if (clienteDTO.Id == ConstantesGerais.ZERO)
            {
                Validate(clienteDTO, Activator.CreateInstance<ClienteDTOValidator>());

                var cliente = _clienteConverter.Conversion(clienteDTO);

                cliente.DataCadastro = DateTime.Now;

                cliente.DataUltimaAlteracao = DateTime.Now;

                _repository.Insert(cliente);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSer_0.GetMessage("matrícula do cliente"));
            }
        }

        public void Put<ClienteDTOValidator>(int id, ClienteDTO clienteDTO) where ClienteDTOValidator : AbstractValidator<ClienteDTO>
        {
            var cliente = _repository.SelectById(id);

            if (clienteDTO.Id > ConstantesGerais.ZERO && clienteDTO.Id == cliente.Id)
            {
                Validate(clienteDTO, Activator.CreateInstance<ClienteDTOValidator>());

                cliente.Nome = clienteDTO.Nome;
                cliente.Sexo = clienteDTO.Sexo;
                cliente.CPF = clienteDTO.CPF;
                cliente.NumeroTelefoneCelular = clienteDTO.NumeroTelefoneCelular;

                cliente.DataCadastro = GetRegistrationDateById(cliente.Id);

                cliente.DataUltimaAlteracao = DateTime.Now;

                _repository.Update(cliente);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do cliente"));
            }
        }

        public void Patch(int id, bool status)
        {
            var cliente = _repository.SelectById(id);

            if (cliente == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Cliente"));

            _statusValidator.ValidateStatus(status);
           
            cliente.Status = status;

            cliente.DataUltimaAlteracao = DateTime.Now;

            _repository.Update(cliente);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do cliente"));
            }
        }

        public ClienteDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new ClienteDTO();

                var cliente = _repository.SelectById(id);

                ClienteDTO clienteDTO = _clienteConverter.Conversion(cliente);

                return clienteDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do cliente"));
            }
        }

        public IList<ClienteDTO> GetAll()
        {
            IList<ClienteDTO> listaClientesDTO;

            var clientes = _repository.SelectAll();

            listaClientesDTO = _clienteConverter.ListConversion(clientes);

            return listaClientesDTO;
        }

        public ICollection<ResponseClienteDTO> GetAll(int pageIndex, int pageSize, ClienteFilter clienteFilter)
        {
            ICollection<ResponseClienteDTO> listaResponseClientesDTO;

            var clientes = _clienteRepository.SelectAll(pageIndex, pageSize, clienteFilter);

            listaResponseClientesDTO = _responseClienteConverter.CollectionConversion(clientes);

            return listaResponseClientesDTO;
        }

        private void Validate(ClienteDTO clienteDTO, AbstractValidator<ClienteDTO> validator)
        {
            if (clienteDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Cliente"));

            validator.ValidateAndThrow(clienteDTO);
        }

        private DateTime GetRegistrationDateById(int id)
        {
            return _clienteRepository.SelectRegistrationDateById(id);
        }
    }
}