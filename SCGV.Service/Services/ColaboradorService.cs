﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class ColaboradorService : IColaboradorService
    {
        private readonly IBaseRepository<Colaborador> _repository;

        private readonly IColaboradorRepository _colaboradorRepository;

        private readonly IColaboradorConverter _colaboradorConverter;

        private readonly IResponseColaboradorConverter _responseColaboradorConverter;

        private StatusValidator _statusValidator = new StatusValidator();

        public ColaboradorService(IBaseRepository<Colaborador> repository, IColaboradorRepository colaboradorRepository, IColaboradorConverter colaboradorConverter, IResponseColaboradorConverter responseColaboradorConverter)
        {
            _repository = repository;
            _colaboradorRepository = colaboradorRepository;
            _colaboradorConverter = colaboradorConverter;
            _responseColaboradorConverter = responseColaboradorConverter;
        }

        public void Post<ColaboradorDTOValidator>(ColaboradorDTO colaboradorDTO) where ColaboradorDTOValidator : AbstractValidator<ColaboradorDTO>
        {
            if (colaboradorDTO.Id == ConstantesGerais.ZERO)
            {
                Validate(colaboradorDTO, Activator.CreateInstance<ColaboradorDTOValidator>());

                var colaborador = _colaboradorConverter.Conversion(colaboradorDTO);

                colaborador.DataCadastro = DateTime.Now;

                colaborador.DataUltimaAlteracao = DateTime.Now;

                _repository.Insert(colaborador);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSer_0.GetMessage("matrícula do colaborador"));
            }
        }

        public void Put<ColaboradorDTOValidator>(int id, ColaboradorDTO colaboradorDTO) where ColaboradorDTOValidator : AbstractValidator<ColaboradorDTO>
        {
            var colaborador = _repository.SelectById(id);

            if (colaboradorDTO.Id > ConstantesGerais.ZERO && colaboradorDTO.Id == colaborador.Id)
            {
                Validate(colaboradorDTO, Activator.CreateInstance<ColaboradorDTOValidator>());

                colaborador.Nome = colaboradorDTO.Nome;
                colaborador.Sexo = colaboradorDTO.Sexo;
                colaborador.CPF = colaboradorDTO.CPF;
                colaborador.NumeroTelefoneCelular = colaboradorDTO.NumeroTelefoneCelular;
                colaborador.Funcao = colaboradorDTO.Funcao;

                colaborador.DataCadastro = GetRegistrationDateById(colaborador.Id);

                colaborador.DataUltimaAlteracao = DateTime.Now;

                _repository.Update(colaborador);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do colaborador"));
            }
        }

        public void Patch(int id, bool status)
        {
            var colaborador = _repository.SelectById(id);

            if (colaborador == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Colaborador"));

            _statusValidator.ValidateStatus(status);

            colaborador.Status = status;

            colaborador.DataUltimaAlteracao = DateTime.Now;

            _repository.Update(colaborador);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do colaborador"));
            }
        }

        public ColaboradorDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new ColaboradorDTO();

                var colaborador = _repository.SelectById(id);

                ColaboradorDTO colaboradorDTO = _colaboradorConverter.Conversion(colaborador);

                return colaboradorDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.MatriculaDeveSerMaiorQue_0.GetMessage("matrícula do colaborador"));
            }
        }

        public IList<ColaboradorDTO> GetAll()
        {
            IList<ColaboradorDTO> listaColaboradoresDTO;

            var colaboradores = _repository.SelectAll();

            listaColaboradoresDTO = _colaboradorConverter.ListConversion(colaboradores);

            return listaColaboradoresDTO;
        }

        public ICollection<ResponseColaboradorDTO> GetAll(int pageIndex, int pageSize, ColaboradorFilter colaboradorFilter)
        {
            ICollection<ResponseColaboradorDTO> listaResponseColaboradoresDTO;

            var colaboradores = _colaboradorRepository.SelectAll(pageIndex, pageSize, colaboradorFilter);

            listaResponseColaboradoresDTO = _responseColaboradorConverter.CollectionConversion(colaboradores);

            return listaResponseColaboradoresDTO;
        }

        private void Validate(ColaboradorDTO colaboradorDTO, AbstractValidator<ColaboradorDTO> validator)
        {
            if (colaboradorDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Colaborador"));

            validator.ValidateAndThrow(colaboradorDTO);
        }

        private DateTime GetRegistrationDateById(int id)
        {
            return _colaboradorRepository.SelectRegistrationDateById(id);
        }
    }
}