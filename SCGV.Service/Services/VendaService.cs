﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using SCGV.Service.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class VendaService : IVendaService
    {
        private readonly IBaseRepository<Venda> _repository;

        private readonly IVendaRepository _vendaRepository;

        private readonly IVendaConverter _vendaConverter;

        private readonly IResponseVendaConverter _responseVendaConverter;

        private readonly IVendaServicoService _vendaServicoService;

        private readonly IVendaProdutoService _vendaProdutoService;

        private readonly IServicoRepository _servicoRepository;

        private readonly IProdutoRepository _produtoRepository;

        private StatusValidator _statusValidator = new StatusValidator();

        public VendaService(IBaseRepository<Venda> repository, IVendaRepository vendaRepository, IVendaConverter vendaConverter, IResponseVendaConverter responseVendaConverter, IVendaServicoService vendaServicoService, IVendaProdutoService vendaProdutoService, IServicoRepository servicoRepository, IProdutoRepository produtoRepository)
        {
            _repository = repository;
            _vendaRepository = vendaRepository;
            _vendaConverter = vendaConverter;
            _responseVendaConverter = responseVendaConverter;
            _vendaServicoService = vendaServicoService;
            _vendaProdutoService = vendaProdutoService;
            _servicoRepository = servicoRepository;
            _produtoRepository = produtoRepository;
        }

        public void Post<VendaDTOValidator>(VendaDTO vendaDTO, List<int> listaServicos, List<int> listaProdutos) where VendaDTOValidator : AbstractValidator<VendaDTO>
        {
            if (vendaDTO.Id == ConstantesGerais.ZERO && (listaServicos.Count > 0 || listaProdutos.Count > 0))
            {
                Validate(vendaDTO, Activator.CreateInstance<VendaDTOValidator>());

                var venda = _vendaConverter.Conversion(vendaDTO);

                _repository.Insert(venda);

                foreach (int codigoDoServico in listaServicos)
                {
                    var servico = _servicoRepository.SelectById(codigoDoServico);

                    vendaDTO.Total += Convert.ToDecimal(ConstantesGerais.UM * servico.Preco);

                    venda.Total = vendaDTO.Total;

                    _repository.Update(venda);

                    VendaServicoDTO vendaServicoDTO = new VendaServicoDTO();

                    vendaServicoDTO.IdVenda = venda.Id;
                    vendaServicoDTO.IdServico = codigoDoServico;
                    vendaServicoDTO.Quantidade = ConstantesGerais.UM;
                    vendaServicoDTO.Valor = servico.Preco;

                    _vendaServicoService.Post<VendaServicoDTOValidator>(vendaServicoDTO);
                }

                foreach (int codigoDoProduto in listaProdutos)
                {
                    var produto = _produtoRepository.SelectById(codigoDoProduto);

                    vendaDTO.Total += Convert.ToDecimal(ConstantesGerais.UM * produto.PrecoUnitario);

                    venda.Total = vendaDTO.Total;

                    _repository.Update(venda);

                    VendaProdutoDTO vendaProdutoDTO = new VendaProdutoDTO();

                    vendaProdutoDTO.IdVenda = venda.Id;
                    vendaProdutoDTO.IdProduto = codigoDoProduto;
                    vendaProdutoDTO.Quantidade = ConstantesGerais.UM;
                    vendaProdutoDTO.Valor = produto.PrecoUnitario;

                    _vendaProdutoService.Post<VendaProdutoDTOValidator>(vendaProdutoDTO);
                }
            }
            else
            {
                throw new Exception(MensagensGerais.DadosInseridosEstaoIncorretos.GetMessage());
            }
        }

        public void Put<VendaDTOValidator>(VendaDTO vendaDTO) where VendaDTOValidator : AbstractValidator<VendaDTO>
        {
            if (vendaDTO.Id > ConstantesGerais.ZERO)
            {
                Validate(vendaDTO, Activator.CreateInstance<VendaDTOValidator>());

                var venda = _vendaConverter.Conversion(vendaDTO);

                _repository.Update(venda);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código da venda"));
            }
        }

        public void Patch(int id, bool status)
        {
            var venda = _repository.SelectById(id);

            if (venda == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Venda"));

            _statusValidator.ValidateStatus(status);

            venda.Status = status;

            _repository.Update(venda);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código da venda"));
            }
        }

        public VendaDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new VendaDTO();

                var venda = _repository.SelectById(id);

                VendaDTO vendaDTO = _vendaConverter.Conversion(venda);

                return vendaDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código da venda"));
            }
        }

        public IList<VendaDTO> GetAll()
        {
            IList<VendaDTO> listaVendasDTO;

            var vendas = _repository.SelectAll();

            listaVendasDTO = _vendaConverter.ListConversion(vendas);

            return listaVendasDTO;
        }

        public ICollection<ResponseVendaDTO> GetAll(int pageIndex, int pageSize)
        {
            ICollection<ResponseVendaDTO> listaResponseVendasDTO;

            var vendas = _vendaRepository.SelectAll(pageIndex, pageSize);

            listaResponseVendasDTO = _responseVendaConverter.CollectionConversion(vendas);

            return listaResponseVendasDTO;
        }

        private void Validate(VendaDTO vendaDTO, AbstractValidator<VendaDTO> validator)
        {
            if (vendaDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Venda"));

            validator.ValidateAndThrow(vendaDTO);
        }
    }
}