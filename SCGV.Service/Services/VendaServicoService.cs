﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class VendaServicoService : IVendaServicoService
    {
        private readonly IBaseRepository<VendaServico> _repository;

        private readonly IVendaServicoRepository _vendaServicoRepository;

        private readonly IVendaServicoConverter _vendaServicoConverter;

        private readonly IResponseVendaServicoConverter _responseVendaServicoConverter;

        public VendaServicoService(IBaseRepository<VendaServico> repository, IVendaServicoRepository vendaServicoRepository, IVendaServicoConverter vendaServicoConverter, IResponseVendaServicoConverter responseVendaServicoConverter)
        {
            _repository = repository;
            _vendaServicoRepository = vendaServicoRepository;
            _vendaServicoConverter = vendaServicoConverter;
            _responseVendaServicoConverter = responseVendaServicoConverter;
        }

        public void Post<VendaServicoDTOValidator>(VendaServicoDTO vendaServicoDTO) where VendaServicoDTOValidator : AbstractValidator<VendaServicoDTO>
        {
            Validate(vendaServicoDTO, Activator.CreateInstance<VendaServicoDTOValidator>());

            var vendaServico = _vendaServicoConverter.Conversion(vendaServicoDTO);

            _repository.Insert(vendaServico);
        }

        public ICollection<ResponseVendaServicoDTO> GetAll(int pageIndex, int pageSize)
        {
            ICollection<ResponseVendaServicoDTO> listaResponseVendasServicosDTO;

            var vendasServicos = _vendaServicoRepository.SelectAll(pageIndex, pageSize);

            listaResponseVendasServicosDTO = _responseVendaServicoConverter.CollectionConversion(vendasServicos);

            return listaResponseVendasServicosDTO;
        }

        private void Validate(VendaServicoDTO vendaServicoDTO, AbstractValidator<VendaServicoDTO> validator)
        {
            if (vendaServicoDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Venda de serviço"));

            validator.ValidateAndThrow(vendaServicoDTO);
        }
    }
}