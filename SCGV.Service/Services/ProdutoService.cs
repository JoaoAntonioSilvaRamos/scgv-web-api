﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.CrossCutting.Validators;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly IBaseRepository<Produto> _repository;

        private readonly IProdutoRepository _produtoRepository;

        private readonly IProdutoConverter _produtoConverter;

        private readonly IResponseProdutoConverter _responseProdutoConverter;

        private StatusValidator _statusValidator = new StatusValidator();

        public ProdutoService(IBaseRepository<Produto> repository, IProdutoRepository produtoRepository, IProdutoConverter produtoConverter, IResponseProdutoConverter responseProdutoConverter)
        {
            _repository = repository;
            _produtoRepository = produtoRepository;
            _produtoConverter = produtoConverter;
            _responseProdutoConverter = responseProdutoConverter;
        }

        public void Post<ProdutoDTOValidator>(ProdutoDTO produtoDTO) where ProdutoDTOValidator : AbstractValidator<ProdutoDTO>
        {
            if (produtoDTO.Id == ConstantesGerais.ZERO)
            {
                Validate(produtoDTO, Activator.CreateInstance<ProdutoDTOValidator>());

                var produto = _produtoConverter.Conversion(produtoDTO);

                produto.DataCadastro = DateTime.Now;

                produto.DataUltimaAlteracao = DateTime.Now;

                _repository.Insert(produto);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSer_0.GetMessage("código do produto"));
            }
        }

        public void Put<ProdutoDTOValidator>(int id, ProdutoDTO produtoDTO) where ProdutoDTOValidator : AbstractValidator<ProdutoDTO>
        {
            var produto = _repository.SelectById(id);

            if (produtoDTO.Id > ConstantesGerais.ZERO && produtoDTO.Id == produto.Id)
            {
                Validate(produtoDTO, Activator.CreateInstance<ProdutoDTOValidator>());

                produto.CodigoBarras = produtoDTO.CodigoBarras;
                produto.Descricao = produtoDTO.CodigoBarras;
                produto.Quantidade = produtoDTO.Quantidade;
                produto.UnidadeMedida = produtoDTO.UnidadeMedida;
                produto.PrecoUnitario = produtoDTO.PrecoUnitario;
                produto.NotaFiscalCompra = produtoDTO.NotaFiscalCompra;
                produto.IdFornecedor = produtoDTO.IdFornecedor;

                produto.DataCadastro = GetRegistrationDateById(produto.Id);

                produto.DataUltimaAlteracao = DateTime.Now;

                _repository.Update(produto);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do produto"));
            }
        }

        public void Patch(int id, bool status)
        {
            var produto = _repository.SelectById(id);

            if (produto == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Produto"));

            _statusValidator.ValidateStatus(status);

            produto.Status = status;

            produto.DataUltimaAlteracao = DateTime.Now;

            _repository.Update(produto);
        }

        public void Delete(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _repository.Delete(id);
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do produto"));
            }
        }

        public ProdutoDTO GetById(int id)
        {
            if (id > ConstantesGerais.ZERO)
            {
                _ = new ProdutoDTO();

                var produto = _repository.SelectById(id);

                ProdutoDTO produtoDTO = _produtoConverter.Conversion(produto);

                return produtoDTO;
            }
            else
            {
                throw new Exception(MensagensGerais.CodigoDeveSerMaiorQue_0.GetMessage("código do produto"));
            }
        }

        public IList<ProdutoDTO> GetAll()
        {
            IList<ProdutoDTO> listaProdutosDTO;

            var produtos = _repository.SelectAll();

            listaProdutosDTO = _produtoConverter.ListConversion(produtos);

            return listaProdutosDTO;
        }

        public ICollection<ResponseProdutoDTO> GetAll(int pageIndex, int pageSize, ProdutoFilter produtoFilter)
        {
            ICollection<ResponseProdutoDTO> listaResponseProdutosDTO;

            var produtos = _produtoRepository.SelectAll(pageIndex, pageSize, produtoFilter);

            listaResponseProdutosDTO = _responseProdutoConverter.CollectionConversion(produtos);

            return listaResponseProdutosDTO;
        }

        private void Validate(ProdutoDTO produtoDTO, AbstractValidator<ProdutoDTO> validator)
        {
            if (produtoDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Produto"));

            validator.ValidateAndThrow(produtoDTO);
        }

        private DateTime GetRegistrationDateById(int id)
        {
            return _produtoRepository.SelectRegistrationDateById(id);
        }
    }
}