﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SCGV.Service.Services
{
    public class ArquivoService : IArquivoService
    {
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public ArquivoService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [Obsolete]
        public async Task<IFormFile> Upload(IFormFile arquivo)
        {
            if (arquivo != null && arquivo.Length > 0)
            {
                var caminho = @"C:\Users\joao_\OneDrive\Área de Trabalho\Arquivos SCGV";

                var caminhoParaUpload = _hostingEnvironment.WebRootPath + caminho + @"\";

                if (!Directory.Exists(caminhoParaUpload))
                {
                    Directory.CreateDirectory(caminhoParaUpload);
                }

                var nomeExclusivoDoArquivo = Guid.NewGuid().ToString();

                var nomeDoArquivo = Path.GetFileName(nomeExclusivoDoArquivo + "." + arquivo.FileName.Split(".")[1].ToLower());

                string caminhoCompleto = caminhoParaUpload + nomeDoArquivo;

                caminho = caminho + @"\";

                var caminhoDoArquivo = @".." + Path.Combine(caminho, nomeDoArquivo);

                using (var fluxoDeArquivos = new FileStream(caminhoCompleto, FileMode.Create))
                {
                    await arquivo.CopyToAsync(fluxoDeArquivos);
                }

                return arquivo;
            }
            else
            {
                throw new Exception(MensagensGerais.ArquivoNaoPodeSerNuloOuVazio.GetMessage());
            }
        }
    }
}