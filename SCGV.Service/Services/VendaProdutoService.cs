﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.CrossCutting.Constants;
using System;
using System.Collections.Generic;

namespace SCGV.Service.Services
{
    public class VendaProdutoService : IVendaProdutoService
    {
        private readonly IBaseRepository<VendaProduto> _repository;

        private readonly IVendaProdutoRepository _vendaProdutoRepository;

        private readonly IVendaProdutoConverter _vendaProdutoConverter;

        private readonly IResponseVendaProdutoConverter _responseVendaProdutoConverter;

        public VendaProdutoService(IBaseRepository<VendaProduto> repository, IVendaProdutoRepository vendaProdutoRepository, IVendaProdutoConverter vendaProdutoConverter, IResponseVendaProdutoConverter responseVendaProdutoConverter)
        {
            _repository = repository;
            _vendaProdutoRepository = vendaProdutoRepository;
            _vendaProdutoConverter = vendaProdutoConverter;
            _responseVendaProdutoConverter = responseVendaProdutoConverter;
        }

        public void Post<VendaProdutoDTOValidator>(VendaProdutoDTO vendaProdutoDTO) where VendaProdutoDTOValidator : AbstractValidator<VendaProdutoDTO>
        {
            Validate(vendaProdutoDTO, Activator.CreateInstance<VendaProdutoDTOValidator>());

            var vendaProduto = _vendaProdutoConverter.Conversion(vendaProdutoDTO);

            _repository.Insert(vendaProduto);
        }

        public ICollection<ResponseVendaProdutoDTO> GetAll(int pageIndex, int pageSize)
        {
            ICollection<ResponseVendaProdutoDTO> listaResponseVendasProdutosDTO;

            var vendasProdutos = _vendaProdutoRepository.SelectAll(pageIndex, pageSize);

            listaResponseVendasProdutosDTO = _responseVendaProdutoConverter.CollectionConversion(vendasProdutos);

            return listaResponseVendasProdutosDTO;
        }

        private void Validate(VendaProdutoDTO vendaProdutoDTO, AbstractValidator<VendaProdutoDTO> validator)
        {
            if (vendaProdutoDTO == null)
                throw new Exception(MensagensGerais.ObjetoNaoEncontrado.GetMessage("Venda do produto"));

            validator.ValidateAndThrow(vendaProdutoDTO);
        }
    }
}