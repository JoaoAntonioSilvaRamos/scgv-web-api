﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ResponseVendaConverter : IResponseVendaConverter
    {
        private ClienteConverter _clienteConverter = new ClienteConverter();

        private ColaboradorConverter _colaboradorConverter = new ColaboradorConverter();

        public Venda Conversion(ResponseVendaDTO responseVendaDTO)
        {
            return new Venda
            {
                Id = responseVendaDTO.Id,
                Data = responseVendaDTO.Data,
                IdCliente = responseVendaDTO.IdCliente,
                Cliente = _clienteConverter.Conversion(responseVendaDTO.ClienteDTO),
                IdColaborador = responseVendaDTO.IdColaborador,
                Colaborador = _colaboradorConverter.Conversion(responseVendaDTO.ColaboradorDTO),
                Total = responseVendaDTO.Total,
                Status = responseVendaDTO.Status
            };
        }

        public ResponseVendaDTO Conversion(Venda venda)
        {
            return new ResponseVendaDTO
            {
                Id = venda.Id,
                Data = venda.Data,
                IdCliente = venda.IdCliente,
                ClienteDTO = _clienteConverter.Conversion(venda.Cliente),
                IdColaborador = venda.IdColaborador,
                ColaboradorDTO = _colaboradorConverter.Conversion(venda.Colaborador),
                Total = venda.Total,
                Status = venda.Status
            };
        }

        public IList<Venda> ListConversion(IList<ResponseVendaDTO> responseVendasDTO)
        {
            return responseVendasDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseVendaDTO> ListConversion(IList<Venda> vendas)
        {
            return vendas.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Venda> CollectionConversion(ICollection<ResponseVendaDTO> responseVendasDTO)
        {
            return responseVendasDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseVendaDTO> CollectionConversion(ICollection<Venda> vendas)
        {
            return vendas.Select(item => Conversion(item)).ToList();
        }
    }
}