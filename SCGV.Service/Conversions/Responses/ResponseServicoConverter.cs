﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ResponseServicoConverter : IResponseServicoConverter
    {
        private VendaServicoConverter _vendaServicoConverter = new VendaServicoConverter();

        public Servico Conversion(ResponseServicoDTO responseServicoDTO)
        {
            return new Servico
            {
                Id = responseServicoDTO.Id,
                Descricao = responseServicoDTO.Descricao,
                Preco = responseServicoDTO.Preco,
                Status = responseServicoDTO.Status,
                ListaVendasDeServicosPorServico = _vendaServicoConverter.CollectionConversion(responseServicoDTO.ListaVendasDeServicosPorServico)
            };
        }

        public ResponseServicoDTO Conversion(Servico servico)
        {
            return new ResponseServicoDTO
            {
                Id = servico.Id,
                Descricao = servico.Descricao,
                Preco = servico.Preco,
                Status = servico.Status,
                ListaVendasDeServicosPorServico = _vendaServicoConverter.CollectionConversion(servico.ListaVendasDeServicosPorServico)
            };
        }

        public IList<Servico> ListConversion(IList<ResponseServicoDTO> responseServicosDTO)
        {
            return responseServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseServicoDTO> ListConversion(IList<Servico> servicos)
        {
            return servicos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Servico> CollectionConversion(ICollection<ResponseServicoDTO> responseServicosDTO)
        {
            return responseServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseServicoDTO> CollectionConversion(ICollection<Servico> servicos)
        {
            return servicos.Select(item => Conversion(item)).ToList();
        }
    }
}