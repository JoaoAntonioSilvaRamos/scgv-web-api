﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ResponseVendaProdutoConverter : IResponseVendaProdutoConverter
    {
        private VendaConverter _vendaConverter = new VendaConverter();

        private ProdutoConverter _produtoConverter = new ProdutoConverter();

        private ResponseVendaConverter _responseVendaConverter = new ResponseVendaConverter();

        private ResponseProdutoConverter _responseProdutoConverter = new ResponseProdutoConverter();

        public VendaProduto Conversion(ResponseVendaProdutoDTO responseVendaProdutoDTO)
        {
            return new VendaProduto
            {
                IdVenda = responseVendaProdutoDTO.IdVenda,
                Venda = _vendaConverter.Conversion(responseVendaProdutoDTO.ResponseVendaDTO),
                IdProduto = responseVendaProdutoDTO.IdProduto,
                Produto = _produtoConverter.Conversion(responseVendaProdutoDTO.ResponseProdutoDTO),
                Quantidade = responseVendaProdutoDTO.Quantidade,
                Valor = responseVendaProdutoDTO.Valor
            };
        }

        public ResponseVendaProdutoDTO Conversion(VendaProduto vendaProduto)
        {
            return new ResponseVendaProdutoDTO
            {
                IdVenda = vendaProduto.IdVenda,
                ResponseVendaDTO = _responseVendaConverter.Conversion(vendaProduto.Venda),
                IdProduto = vendaProduto.IdProduto,
                ResponseProdutoDTO = _responseProdutoConverter.Conversion(vendaProduto.Produto),
                Quantidade = vendaProduto.Quantidade,
                Valor = vendaProduto.Valor
            };
        }

        public IList<VendaProduto> ListConversion(IList<ResponseVendaProdutoDTO> responseVendasProdutosDTO)
        {
            return responseVendasProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseVendaProdutoDTO> ListConversion(IList<VendaProduto> vendasProdutos)
        {
            return vendasProdutos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaProduto> CollectionConversion(ICollection<ResponseVendaProdutoDTO> responseVendasProdutosDTO)
        {
            return responseVendasProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseVendaProdutoDTO> CollectionConversion(ICollection<VendaProduto> vendasProdutos)
        {
            return vendasProdutos.Select(item => Conversion(item)).ToList();
        }
    }
}