﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ResponseVendaServicoConverter : IResponseVendaServicoConverter
    {
        private VendaConverter _vendaConverter = new VendaConverter();

        private ServicoConverter _servicoConverter = new ServicoConverter();

        private ResponseVendaConverter _responseVendaConverter = new ResponseVendaConverter();

        private ResponseServicoConverter _responseServicoConverter = new ResponseServicoConverter();

        public VendaServico Conversion(ResponseVendaServicoDTO responseVendaServicoDTO)
        {
            return new VendaServico
            {
                IdVenda = responseVendaServicoDTO.IdVenda,
                Venda = _vendaConverter.Conversion(responseVendaServicoDTO.ResponseVendaDTO),
                IdServico = responseVendaServicoDTO.IdServico,
                Servico = _servicoConverter.Conversion(responseVendaServicoDTO.ResponseServicoDTO),
                Quantidade = responseVendaServicoDTO.Quantidade,
                Valor = responseVendaServicoDTO.Valor
            };
        }

        public ResponseVendaServicoDTO Conversion(VendaServico vendaServico)
        {
            return new ResponseVendaServicoDTO
            {
                IdVenda = vendaServico.IdVenda,
                ResponseVendaDTO = _responseVendaConverter.Conversion(vendaServico.Venda),
                IdServico = vendaServico.IdServico,
                ResponseServicoDTO = _responseServicoConverter.Conversion(vendaServico.Servico),
                Quantidade = vendaServico.Quantidade,
                Valor = vendaServico.Valor
            };
        }

        public IList<VendaServico> ListConversion(IList<ResponseVendaServicoDTO> responseVendasServicosDTO)
        {
            return responseVendasServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseVendaServicoDTO> ListConversion(IList<VendaServico> vendasServicos)
        {
            return vendasServicos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaServico> CollectionConversion(ICollection<ResponseVendaServicoDTO> responseVendasServicosDTO)
        {
            return responseVendasServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseVendaServicoDTO> CollectionConversion(ICollection<VendaServico> vendasServicos)
        {
            return vendasServicos.Select(item => Conversion(item)).ToList();
        }
    }
}