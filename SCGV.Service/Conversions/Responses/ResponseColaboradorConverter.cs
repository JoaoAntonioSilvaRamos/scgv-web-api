﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Service.Conversions;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Responses.Conversions
{
    public class ResponseColaboradorConverter : IResponseColaboradorConverter
    {
        private VendaConverter _vendaConverter = new VendaConverter();

        public Colaborador Conversion(ResponseColaboradorDTO responseColaboradorDTO)
        {
            return new Colaborador
            {
                Id = responseColaboradorDTO.Id,
                Nome = responseColaboradorDTO.Nome,
                Sexo = responseColaboradorDTO.Sexo,
                CPF = responseColaboradorDTO.CPF,
                NumeroTelefoneCelular = responseColaboradorDTO.NumeroTelefoneCelular,
                Funcao = responseColaboradorDTO.Funcao,
                Status = responseColaboradorDTO.Status,
                ListaVendasPorColaborador = _vendaConverter.CollectionConversion(responseColaboradorDTO.ListaVendasPorColaborador)
            };
        }

        public ResponseColaboradorDTO Conversion(Colaborador colaborador)
        {
            return new ResponseColaboradorDTO
            {
                Id = colaborador.Id,
                Nome = colaborador.Nome,
                Sexo = colaborador.Sexo,
                CPF = colaborador.CPF,
                NumeroTelefoneCelular = colaborador.NumeroTelefoneCelular,
                Funcao = colaborador.Funcao,
                Status = colaborador.Status,
                ListaVendasPorColaborador = _vendaConverter.CollectionConversion(colaborador.ListaVendasPorColaborador)
            };
        }

        public IList<Colaborador> ListConversion(IList<ResponseColaboradorDTO> responseColaboradoresDTO)
        {
            return responseColaboradoresDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseColaboradorDTO> ListConversion(IList<Colaborador> colaboradores)
        {
            return colaboradores.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Colaborador> CollectionConversion(ICollection<ResponseColaboradorDTO> responseColaboradoresDTO)
        {
            return responseColaboradoresDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseColaboradorDTO> CollectionConversion(ICollection<Colaborador> colaboradores)
        {
            return colaboradores.Select(item => Conversion(item)).ToList();
        }
    }
}