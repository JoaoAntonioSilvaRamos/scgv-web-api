﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ResponseProdutoConverter : IResponseProdutoConverter
    {
        private FornecedorConverter _fornecedorConverter = new FornecedorConverter();

        private VendaProdutoConverter _vendaProdutoConverter = new VendaProdutoConverter();

        public Produto Conversion(ResponseProdutoDTO responseProdutoDTO)
        {
            return new Produto
            {
                Id = responseProdutoDTO.Id,
                CodigoBarras = responseProdutoDTO.CodigoBarras,
                Descricao = responseProdutoDTO.Descricao,
                Quantidade = responseProdutoDTO.Quantidade,
                UnidadeMedida = responseProdutoDTO.UnidadeMedida,
                PrecoUnitario = responseProdutoDTO.PrecoUnitario,
                NotaFiscalCompra = responseProdutoDTO.NotaFiscalCompra,
                IdFornecedor = responseProdutoDTO.IdFornecedor,
                Fornecedor = _fornecedorConverter.Conversion(responseProdutoDTO.Fornecedor),
                Status = responseProdutoDTO.Status,
                ListaVendasDeProdutosPorProduto = _vendaProdutoConverter.CollectionConversion(responseProdutoDTO.ListaVendasDeProdutosPorProduto)
            };
        }

        public ResponseProdutoDTO Conversion(Produto produto)
        {
            return new ResponseProdutoDTO
            {
                Id = produto.Id,
                CodigoBarras = produto.CodigoBarras,
                Descricao = produto.Descricao,
                Quantidade = produto.Quantidade,
                UnidadeMedida = produto.UnidadeMedida,
                PrecoUnitario = produto.PrecoUnitario,
                NotaFiscalCompra = produto.NotaFiscalCompra,
                IdFornecedor = produto.IdFornecedor,
                Fornecedor = _fornecedorConverter.Conversion(produto.Fornecedor),
                Status = produto.Status,
                ListaVendasDeProdutosPorProduto = _vendaProdutoConverter.CollectionConversion(produto.ListaVendasDeProdutosPorProduto)
            };
        }

        public IList<Produto> ListConversion(IList<ResponseProdutoDTO> responseProdutosDTO)
        {
            return responseProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseProdutoDTO> ListConversion(IList<Produto> produtos)
        {
            return produtos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Produto> CollectionConversion(ICollection<ResponseProdutoDTO> responseProdutosDTO)
        {
            return responseProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseProdutoDTO> CollectionConversion(ICollection<Produto> produtos)
        {
            return produtos.Select(item => Conversion(item)).ToList();
        }
    }
}