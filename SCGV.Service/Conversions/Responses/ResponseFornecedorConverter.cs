﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Service.Conversions;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Responses.Conversions
{
    public class ResponseFornecedorConverter : IResponseFornecedorConverter
    {
        private ProdutoConverter _produtoConverter = new ProdutoConverter();

        public Fornecedor Conversion(ResponseFornecedorDTO responseFornecedorDTO)
        {
            return new Fornecedor
            {
                Id = responseFornecedorDTO.Id,
                RazaoSocial = responseFornecedorDTO.RazaoSocial,
                NomeFantasia = responseFornecedorDTO.NomeFantasia,
                CNPJ = responseFornecedorDTO.CNPJ,
                NumeroTelefoneCelular = responseFornecedorDTO.NumeroTelefoneCelular,
                Status = responseFornecedorDTO.Status,
                ListaProdutosPorFornecedor = _produtoConverter.CollectionConversion(responseFornecedorDTO.ListaProdutosPorFornecedor)
            };
        }

        public ResponseFornecedorDTO Conversion(Fornecedor fornecedor)
        {
            return new ResponseFornecedorDTO
            {
                Id = fornecedor.Id,
                RazaoSocial = fornecedor.RazaoSocial,
                NomeFantasia = fornecedor.NomeFantasia,
                CNPJ = fornecedor.CNPJ,
                NumeroTelefoneCelular = fornecedor.NumeroTelefoneCelular,
                Status = fornecedor.Status,
                ListaProdutosPorFornecedor = _produtoConverter.CollectionConversion(fornecedor.ListaProdutosPorFornecedor)
            };
        }

        public IList<Fornecedor> ListConversion(IList<ResponseFornecedorDTO> responseFornecedoresDTO)
        {
            return responseFornecedoresDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseFornecedorDTO> ListConversion(IList<Fornecedor> fornecedores)
        {
            return fornecedores.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Fornecedor> CollectionConversion(ICollection<ResponseFornecedorDTO> responseFornecedoresDTO)
        {
            return responseFornecedoresDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseFornecedorDTO> CollectionConversion(ICollection<Fornecedor> fornecedores)
        {
            return fornecedores.Select(item => Conversion(item)).ToList();
        }
    }
}