﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Service.Conversions;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Responses.Conversions
{
    public class ResponseClienteConverter : IResponseClienteConverter
    {
        private VendaConverter _vendaConverter = new VendaConverter();

        public Cliente Conversion(ResponseClienteDTO responseClienteDTO)
        {
            return new Cliente
            {
                Id = responseClienteDTO.Id,
                Nome = responseClienteDTO.Nome,
                Sexo = responseClienteDTO.Sexo,
                CPF = responseClienteDTO.CPF,
                NumeroTelefoneCelular = responseClienteDTO.NumeroTelefoneCelular,
                Status = responseClienteDTO.Status,
                ListaVendasPorCliente = _vendaConverter.CollectionConversion(responseClienteDTO.ListaVendasPorCliente)
            };
        }

        public ResponseClienteDTO Conversion(Cliente cliente)
        {
            return new ResponseClienteDTO
            {
                Id = cliente.Id,
                Nome = cliente.Nome,
                Sexo = cliente.Sexo,
                CPF = cliente.CPF,
                NumeroTelefoneCelular = cliente.NumeroTelefoneCelular,
                Status = cliente.Status,
                ListaVendasPorCliente = _vendaConverter.CollectionConversion(cliente.ListaVendasPorCliente)
            };
        }

        public IList<Cliente> ListConversion(IList<ResponseClienteDTO> responseClientesDTO)
        {
            return responseClientesDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ResponseClienteDTO> ListConversion(IList<Cliente> clientes)
        {
            return clientes.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Cliente> CollectionConversion(ICollection<ResponseClienteDTO> responseClientesDTO)
        {
            return responseClientesDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ResponseClienteDTO> CollectionConversion(ICollection<Cliente> clientes)
        {
            return clientes.Select(item => Conversion(item)).ToList();
        }
    }
}