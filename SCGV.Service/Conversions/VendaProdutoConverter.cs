﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class VendaProdutoConverter : IVendaProdutoConverter
    {
        public VendaProduto Conversion(VendaProdutoDTO vendaProdutoDTO)
        {
            return new VendaProduto
            {
                IdVenda = vendaProdutoDTO.IdVenda,
                IdProduto = vendaProdutoDTO.IdProduto,
                Quantidade = vendaProdutoDTO.Quantidade,
                Valor = vendaProdutoDTO.Valor
            };
        }

        public VendaProdutoDTO Conversion(VendaProduto vendaProduto)
        {
            return new VendaProdutoDTO
            {
                IdVenda = vendaProduto.IdVenda,
                IdProduto = vendaProduto.IdProduto,
                Quantidade = vendaProduto.Quantidade,
                Valor = vendaProduto.Valor
            };
        }

        public IList<VendaProduto> ListConversion(IList<VendaProdutoDTO> vendasProdutosDTO)
        {
            return vendasProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<VendaProdutoDTO> ListConversion(IList<VendaProduto> vendasProdutos)
        {
            return vendasProdutos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaProduto> CollectionConversion(ICollection<VendaProdutoDTO> vendasProdutosDTO)
        {
            return vendasProdutosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaProdutoDTO> CollectionConversion(ICollection<VendaProduto> vendasProdutos)
        {
            return vendasProdutos.Select(item => Conversion(item)).ToList();
        }
    }
}