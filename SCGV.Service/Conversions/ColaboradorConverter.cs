﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ColaboradorConverter : IColaboradorConverter
    {
        public Colaborador Conversion(ColaboradorDTO colaboradorDTO)
        {
            return new Colaborador
            {
                Id = colaboradorDTO.Id,
                Nome = colaboradorDTO.Nome,
                Sexo = colaboradorDTO.Sexo,
                CPF = colaboradorDTO.CPF,
                NumeroTelefoneCelular = colaboradorDTO.NumeroTelefoneCelular,
                Funcao = colaboradorDTO.Funcao,
                Status = colaboradorDTO.Status
            };
        }

        public ColaboradorDTO Conversion(Colaborador colaborador)
        {
            return new ColaboradorDTO
            {
                Id = colaborador.Id,
                Nome = colaborador.Nome,
                Sexo = colaborador.Sexo,
                CPF = colaborador.CPF,
                NumeroTelefoneCelular = colaborador.NumeroTelefoneCelular,
                Funcao = colaborador.Funcao,
                Status = colaborador.Status
            };
        }

        public IList<Colaborador> ListConversion(IList<ColaboradorDTO> colaboradoresDTO)
        {
            return colaboradoresDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ColaboradorDTO> ListConversion(IList<Colaborador> colaboradores)
        {
            return colaboradores.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Colaborador> CollectionConversion(ICollection<ColaboradorDTO> colaboradoresDTO)
        {
            return colaboradoresDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ColaboradorDTO> CollectionConversion(ICollection<Colaborador> colaboradores)
        {
            return colaboradores.Select(item => Conversion(item)).ToList();
        }
    }
}