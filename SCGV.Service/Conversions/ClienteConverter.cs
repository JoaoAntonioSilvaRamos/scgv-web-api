﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ClienteConverter : IClienteConverter
    {
        public Cliente Conversion(ClienteDTO clienteDTO)
        {
            return new Cliente
            {
                Id = clienteDTO.Id,
                Nome = clienteDTO.Nome,
                Sexo = clienteDTO.Sexo,
                CPF = clienteDTO.CPF,
                NumeroTelefoneCelular = clienteDTO.NumeroTelefoneCelular,
                Status = clienteDTO.Status
            };
        }

        public ClienteDTO Conversion(Cliente cliente)
        {
            return new ClienteDTO
            {
                Id = cliente.Id,
                Nome = cliente.Nome,
                Sexo = cliente.Sexo,
                CPF = cliente.CPF,
                NumeroTelefoneCelular = cliente.NumeroTelefoneCelular,
                Status = cliente.Status
            };
        }

        public IList<Cliente> ListConversion(IList<ClienteDTO> clientesDTO)
        {
            return clientesDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ClienteDTO> ListConversion(IList<Cliente> clientes)
        {
            return clientes.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Cliente> CollectionConversion(ICollection<ClienteDTO> clientesDTO)
        {
            return clientesDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ClienteDTO> CollectionConversion(ICollection<Cliente> clientes)
        {
            return clientes.Select(item => Conversion(item)).ToList();
        }
    }
}