﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class VendaServicoConverter : IVendaServicoConverter
    {
        public VendaServico Conversion(VendaServicoDTO vendaServicoDTO)
        {
            return new VendaServico
            {
                IdVenda = vendaServicoDTO.IdVenda,
                IdServico = vendaServicoDTO.IdServico,
                Quantidade = vendaServicoDTO.Quantidade,
                Valor = vendaServicoDTO.Valor
            };
        }

        public VendaServicoDTO Conversion(VendaServico vendaServico)
        {
            return new VendaServicoDTO
            {
                IdVenda = vendaServico.IdVenda,
                IdServico = vendaServico.IdServico,
                Quantidade = vendaServico.Quantidade,
                Valor = vendaServico.Valor
            };
        }

        public IList<VendaServico> ListConversion(IList<VendaServicoDTO> vendasServicosDTO)
        {
            return vendasServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<VendaServicoDTO> ListConversion(IList<VendaServico> vendasServicos)
        {
            return vendasServicos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaServico> CollectionConversion(ICollection<VendaServicoDTO> vendasServicosDTO)
        {
            return vendasServicosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaServicoDTO> CollectionConversion(ICollection<VendaServico> vendasServicos)
        {
            return vendasServicos.Select(item => Conversion(item)).ToList();
        }
    }
}