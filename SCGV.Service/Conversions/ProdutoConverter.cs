﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ProdutoConverter : IProdutoConverter
    {
        public Produto Conversion(ProdutoDTO produtoDTO)
        {
            return new Produto
            {
                Id = produtoDTO.Id,
                CodigoBarras = produtoDTO.CodigoBarras,
                Descricao = produtoDTO.Descricao,
                Quantidade = produtoDTO.Quantidade,
                UnidadeMedida = produtoDTO.UnidadeMedida,
                PrecoUnitario = produtoDTO.PrecoUnitario,
                NotaFiscalCompra = produtoDTO.NotaFiscalCompra,
                IdFornecedor = produtoDTO.IdFornecedor,
                Status = produtoDTO.Status
            };
        }

        public ProdutoDTO Conversion(Produto produto)
        {
            return new ProdutoDTO
            {
                Id = produto.Id,
                CodigoBarras = produto.CodigoBarras,
                Descricao = produto.Descricao,
                Quantidade = produto.Quantidade,
                UnidadeMedida = produto.UnidadeMedida,
                PrecoUnitario = produto.PrecoUnitario,
                NotaFiscalCompra = produto.NotaFiscalCompra,
                IdFornecedor = produto.IdFornecedor,
                Status = produto.Status
            };
        }

        public IList<Produto> ListConversion(IList<ProdutoDTO> produtosDTO)
        {
            return produtosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ProdutoDTO> ListConversion(IList<Produto> produtos)
        {
            return produtos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Produto> CollectionConversion(ICollection<ProdutoDTO> produtosDTO)
        {
            return produtosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ProdutoDTO> CollectionConversion(ICollection<Produto> produtos)
        {
            return produtos.Select(item => Conversion(item)).ToList();
        }
    }
}