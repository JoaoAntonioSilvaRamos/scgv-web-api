﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class ServicoConverter : IServicoConverter
    {
        public Servico Conversion(ServicoDTO servicoDTO)
        {
            return new Servico
            {
                Id = servicoDTO.Id,
                Descricao = servicoDTO.Descricao,
                Preco = servicoDTO.Preco,
                Status = servicoDTO.Status
            };
        }

        public ServicoDTO Conversion(Servico servico)
        {
            return new ServicoDTO
            {
                Id = servico.Id,
                Descricao = servico.Descricao,
                Preco = servico.Preco,
                Status = servico.Status
            };
        }

        public IList<Servico> ListConversion(IList<ServicoDTO> servicosDTO)
        {
            return servicosDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<ServicoDTO> ListConversion(IList<Servico> servicos)
        {
            return servicos.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Servico> CollectionConversion(ICollection<ServicoDTO> servicosDTO)
        {
            return servicosDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<ServicoDTO> CollectionConversion(ICollection<Servico> servicos)
        {
            return servicos.Select(item => Conversion(item)).ToList();
        }
    }
}