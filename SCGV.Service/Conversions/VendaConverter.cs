﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class VendaConverter : IVendaConverter
    {
        public Venda Conversion(VendaDTO vendaDTO)
        {
            return new Venda
            {
                Id = vendaDTO.Id,
                Data = vendaDTO.Data,
                IdCliente = vendaDTO.IdCliente,
                IdColaborador = vendaDTO.IdColaborador,
                Total = vendaDTO.Total,
                Status = vendaDTO.Status
            };
        }

        public VendaDTO Conversion(Venda venda)
        {
            return new VendaDTO
            {
                Id = venda.Id,
                Data = venda.Data,
                IdCliente = venda.IdCliente,
                IdColaborador = venda.IdColaborador,
                Total = venda.Total,
                Status = venda.Status
            };
        }

        public IList<Venda> ListConversion(IList<VendaDTO> vendasDTO)
        {
            return vendasDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<VendaDTO> ListConversion(IList<Venda> vendas)
        {
            return vendas.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Venda> CollectionConversion(ICollection<VendaDTO> vendasDTO)
        {
            return vendasDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<VendaDTO> CollectionConversion(ICollection<Venda> vendas)
        {
            return vendas.Select(item => Conversion(item)).ToList();
        }
    }
}