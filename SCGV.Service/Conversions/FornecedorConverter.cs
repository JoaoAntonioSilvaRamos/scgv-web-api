﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Service.Conversions
{
    public class FornecedorConverter : IFornecedorConverter
    {
        public Fornecedor Conversion(FornecedorDTO fornecedorDTO)
        {
            return new Fornecedor
            {
                Id = fornecedorDTO.Id,
                RazaoSocial = fornecedorDTO.RazaoSocial,
                NomeFantasia = fornecedorDTO.NomeFantasia,
                CNPJ = fornecedorDTO.CNPJ,
                NumeroTelefoneCelular = fornecedorDTO.NumeroTelefoneCelular,
                Status = fornecedorDTO.Status
            };
        }

        public FornecedorDTO Conversion(Fornecedor fornecedor)
        {
            return new FornecedorDTO
            {
                Id = fornecedor.Id,
                RazaoSocial = fornecedor.RazaoSocial,
                NomeFantasia = fornecedor.NomeFantasia,
                CNPJ = fornecedor.CNPJ,
                NumeroTelefoneCelular = fornecedor.NumeroTelefoneCelular,
                Status = fornecedor.Status
            };
        }

        public IList<Fornecedor> ListConversion(IList<FornecedorDTO> fornecedoresDTO)
        {
            return fornecedoresDTO.Select(item => Conversion(item)).ToList();
        }

        public IList<FornecedorDTO> ListConversion(IList<Fornecedor> fornecedores)
        {
            return fornecedores.Select(item => Conversion(item)).ToList();
        }

        public ICollection<Fornecedor> CollectionConversion(ICollection<FornecedorDTO> fornecedoresDTO)
        {
            return fornecedoresDTO.Select(item => Conversion(item)).ToList();
        }

        public ICollection<FornecedorDTO> CollectionConversion(ICollection<Fornecedor> fornecedores)
        {
            return fornecedores.Select(item => Conversion(item)).ToList();
        }
    }
}