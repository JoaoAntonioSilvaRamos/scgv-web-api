﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class ServicoDTOValidator : AbstractValidator<ServicoDTO>
    {
        public ServicoDTOValidator()
        {
            RuleFor(s => s)
                .NotNull()
                .OnAnyFailure(s =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar o serviço!");
                });

            RuleFor(s => s.Descricao)
                .NotEmpty().WithMessage("A descrição do serviço não pode ser vazia!")
                .NotNull().WithMessage("A descrição do serviço não pode ser nula!")
                .Length(1,255).WithMessage("A descrição do serviço deve ter entre 1 e 255 caracteres!");

            RuleFor(s => s.Preco)
                .NotEmpty().WithMessage("O preço do serviço não pode ser vazio!")
                .NotNull().WithMessage("O preço do serviço não pode ser nulo!")
                .GreaterThan(0).WithMessage("O preço do serviço deve ser maior que 0!");

            RuleFor(s => s.Status)
                .NotEmpty().WithMessage("O status do serviço não pode ser vazio!")
                .NotNull().WithMessage("O status do serviço não pode ser nulo!");
        }
    }
}