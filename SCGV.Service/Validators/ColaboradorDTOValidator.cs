﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class ColaboradorDTOValidator : AbstractValidator<ColaboradorDTO>
    {
        public ColaboradorDTOValidator()
        {
            RuleFor(c => c)
                .NotNull()
                .OnAnyFailure(c =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar o colaborador!");
                });

            RuleFor(c => c.Nome)
                .NotEmpty().WithMessage("O nome do colaborador não pode ser vazio!")
                .NotNull().WithMessage("O nome do colaborador não pode ser nulo!")
                .Length(1,255).WithMessage("O nome do colaborador deve ter entre 1 e 255 caracteres!");

            RuleFor(c => c.Sexo)
                .NotEmpty().WithMessage("O sexo do colaborador não pode ser vazio!")
                .NotNull().WithMessage("O sexo do colaborador não pode ser nulo!")
                .MaximumLength(1).WithMessage("O sexo do colaborador deve ser M ou F!");

            RuleFor(c => c.CPF)
                .NotEmpty().WithMessage("O CPF do colaborador não pode ser vazio!")
                .NotNull().WithMessage("O CPF do colaborador não pode ser nulo!")
                .Length(14).WithMessage("O CPF do colaborador deve conter 14 caracteres, " +
                "nesse formato 000.000.000-00");

            RuleFor(c => c.NumeroTelefoneCelular)
                .NotEmpty().WithMessage("O número do telefone celular do colaborador não pode ser vazio!")
                .NotNull().WithMessage("O número do telefone celular do colaborador não pode ser nulo!")
                .Length(13).WithMessage("O número do telefone celular do colaborador deve conter 13 caracteres, " +
                "nesse formato 00 00000-0000");

            RuleFor(c => c.Funcao)
                .NotEmpty().WithMessage("A função do colaborador não pode ser vazia!")
                .NotNull().WithMessage("A função do colaborador não pode ser nula!")
                .Length(1,100).WithMessage("A função do colaborador deve ter entre 1 e 100 caracteres!");

            RuleFor(c => c.Status)
                .NotEmpty().WithMessage("O status do colaborador não pode ser vazio!")
                .NotNull().WithMessage("O status do colaborador não pode ser nulo!");
        }
    }
}