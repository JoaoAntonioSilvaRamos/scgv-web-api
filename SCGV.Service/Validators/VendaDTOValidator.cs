﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class VendaDTOValidator : AbstractValidator<VendaDTO>
    {
        public VendaDTOValidator()
        {
            RuleFor(v => v)
                .NotNull()
                .OnAnyFailure(v =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar a venda!");
                });

            RuleFor(v => v.Data)
                .NotEmpty().WithMessage("A data da venda não pode ser vazia!")
                .NotNull().WithMessage("A data da venda não pode ser nula!");

            RuleFor(v => v.IdCliente)
                .NotEmpty().WithMessage("A matrícula do cliente não pode ser vazia!")
                .NotNull().WithMessage("A matrícula do cliente não pode ser nula!")
                .GreaterThan(0).WithMessage("A matrícula do cliente deve ser maior que 0!");

            RuleFor(v => v.IdColaborador)
                .NotEmpty().WithMessage("A matrícula do colaborador não pode ser vazia!")
                .NotNull().WithMessage("A matrícula do colaborador não pode ser nula!")
                .GreaterThan(0).WithMessage("A matrícula do colaborador deve ser maior que 0!");

            RuleFor(v => v.Status)
                .NotEmpty().WithMessage("O status da venda não pode ser vazio!")
                .NotNull().WithMessage("O status da venda não pode ser nulo!");
        }
    }
}