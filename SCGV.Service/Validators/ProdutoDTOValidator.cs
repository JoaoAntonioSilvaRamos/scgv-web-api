﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class ProdutoDTOValidator : AbstractValidator<ProdutoDTO>
    {
        public ProdutoDTOValidator()
        {
            RuleFor(p => p)
                .NotNull()
                .OnAnyFailure(p =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar o produto!");
                });

            RuleFor(p => p.CodigoBarras)
                .NotEmpty().WithMessage("O código de barras do produto não pode ser vazio!")
                .NotNull().WithMessage("O código de barras do produto não pode ser nulo!")
                .Length(1,255).WithMessage("O código de barras do produto deve ter entre 1 e 255 caracteres!");

            RuleFor(p => p.Descricao)
                .NotEmpty().WithMessage("A descrição do produto não pode ser vazia!")
                .NotNull().WithMessage("A descrição do produto não pode ser nula!")
                .Length(1,255).WithMessage("A descrição do produto deve ter entre 1 e 255 caracteres!");

            RuleFor(p => p.Quantidade)
                .NotEmpty().WithMessage("A quantidade do produto não pode ser vazia!")
                .NotNull().WithMessage("A quantidade do produto não pode ser nula!")
                .GreaterThan(0).WithMessage("A quantidade do produto deve ser maior que 0!");

            RuleFor(p => p.UnidadeMedida)
                .NotEmpty().WithMessage("A unidade de medida do produto não pode ser vazia!")
                .NotNull().WithMessage("A unidade de medida do produto não pode ser nula!")
                .Length(1,5).WithMessage("A unidade de medida do produto deve ter entre 1 e 5 caracteres!");

            RuleFor(p => p.PrecoUnitario)
                .NotEmpty().WithMessage("O preço unitário do produto não pode ser vazio!")
                .NotNull().WithMessage("O preço unitário do produto não pode ser nulo!")
                .GreaterThan(0).WithMessage("O preço unitário do produto deve ser maior que 0!");

            RuleFor(p => p.NotaFiscalCompra)
                .NotEmpty().WithMessage("A nota fiscal de compra do produto não pode ser vazia!")
                .NotNull().WithMessage("A nota fiscal de compra do produto não pode ser nula!")
                .Length(1,10).WithMessage("A nota fiscal de compra do produto deve ter entre 1 e 10 caracteres!");

            RuleFor(p => p.IdFornecedor)
                .NotEmpty().WithMessage("A matrícula do fornecedor não pode ser vazia!")
                .NotNull().WithMessage("A matrícula do fornecedor não pode ser nula!")
                .GreaterThan(0).WithMessage("A matrícula do fornecedor deve ser maior que 0!");

            RuleFor(p => p.Status)
                .NotEmpty().WithMessage("O status do produto não pode ser vazio!")
                .NotNull().WithMessage("O status do produto não pode ser nulo!");
        }
    }
}