﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class FornecedorDTOValidator : AbstractValidator<FornecedorDTO>
    {
        public FornecedorDTOValidator()
        {
            RuleFor(f => f)
                .NotNull()
                .OnAnyFailure(f =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar o fornecedor!");
                });

            RuleFor(f => f.RazaoSocial)
                .NotEmpty().WithMessage("A razão social do fornecedor não pode ser vazia!")
                .NotNull().WithMessage("A razão social do fornecedor não pode ser nula!")
                .Length(1,255).WithMessage("A razão social do fornecedor deve ter entre 1 e 255 caracteres!");

            RuleFor(f => f.NomeFantasia)
                .NotEmpty().WithMessage("O nome fantasia do fornecedor não pode ser vazio!")
                .NotNull().WithMessage("O nome fantasia do fornecedor não pode ser nulo!")
                .Length(1,255).WithMessage("O nome fantasia do fornecedor deve ter entre 1 e 255 caracteres!");

            RuleFor(f => f.CNPJ)
                .NotEmpty().WithMessage("O CNPJ do fornecedor não pode ser vazio!")
                .NotNull().WithMessage("O CNPJ do fornecedor não pode ser nulo!")
                .Length(18).WithMessage("O CNPJ do fornecedor deve conter 18 caracteres, " +
                "nesse formato 00.000.000/0000-00");

            RuleFor(f => f.NumeroTelefoneCelular)
                .NotEmpty().WithMessage("O número do telefone celular do fornecedor não pode ser vazio!")
                .NotNull().WithMessage("O número do telefone celular do fornecedor não pode ser nulo!")
                .Length(13).WithMessage("O número do telefone celular do fornecedor deve conter 13 caracteres, " +
                "nesse formato 00 00000-0000");

            RuleFor(f => f.Status)
                .NotEmpty().WithMessage("O status do fornecedor não pode ser vazio!")
                .NotNull().WithMessage("O status do fornecedor não pode ser nulo!");
        }
    }
}