﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class VendaServicoDTOValidator : AbstractValidator<VendaServicoDTO>
    {
        public VendaServicoDTOValidator()
        {
            RuleFor(vs => vs)
                .NotNull()
                .OnAnyFailure(vs =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar a venda de serviço!");
                });

            RuleFor(vs => vs.IdVenda)
                .NotEmpty().WithMessage("O código da venda não pode ser vazio!")
                .NotNull().WithMessage("O código da venda não pode ser nulo!")
                .GreaterThan(0).WithMessage("O código da venda deve ser maior que 0!");

            RuleFor(vs => vs.IdServico)
                .NotEmpty().WithMessage("O código do serviço não pode ser vazio!")
                .NotNull().WithMessage("O código do serviço não pode ser nulo!")
                .GreaterThan(0).WithMessage("O código do serviço deve ser maior que 0!");

            RuleFor(vs => vs.Quantidade)
                .NotEmpty().WithMessage("A quantidade de serviço realizado não pode ser vazia!")
                .NotNull().WithMessage("A quantidade de serviço realizado não pode ser nula!")
                .GreaterThan(0).WithMessage("A quantidade de serviço realizado deve ser maior que 0!");

            RuleFor(vs => vs.Valor)
                .NotEmpty().WithMessage("O valor do serviço realizado não pode ser vazio!")
                .NotNull().WithMessage("O valor do serviço realizado não pode ser nulo!")
                .GreaterThan(0).WithMessage("O valor do serviço realizado deve ser maior que 0!");
        }
    }
}