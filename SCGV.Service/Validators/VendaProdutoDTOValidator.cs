﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System;

namespace SCGV.Service.Validators
{
    public class VendaProdutoDTOValidator : AbstractValidator<VendaProdutoDTO>
    {
        public VendaProdutoDTOValidator()
        {
            RuleFor(vp => vp)
                .NotNull()
                .OnAnyFailure(vp =>
                {
                    throw new ArgumentNullException("Não foi possível encontrar a venda de produto!");
                });

            RuleFor(vp => vp.IdVenda)
                .NotEmpty().WithMessage("O código da venda não pode ser vazio!")
                .NotNull().WithMessage("O código da venda não pode ser nulo!")
                .GreaterThan(0).WithMessage("O código da venda deve ser maior que 0!");

            RuleFor(vp => vp.IdProduto)
                .NotEmpty().WithMessage("O código do produto não pode ser vazio!")
                .NotNull().WithMessage("O código do produto não pode ser nulo!")
                .GreaterThan(0).WithMessage("O código do produto deve ser maior que 0!");

            RuleFor(vp => vp.Quantidade)
                .NotEmpty().WithMessage("A quantidade de produto vendido não pode ser vazia!")
                .NotNull().WithMessage("A quantidade de produto vendido não pode ser nula!")
                .GreaterThan(0).WithMessage("A quantidade de produto vendido deve ser maior que 0!");

            RuleFor(vp => vp.Valor)
                .NotEmpty().WithMessage("O valor do produto vendido não pode ser vazio!")
                .NotNull().WithMessage("O valor do produto vendido não pode ser nulo!")
                .GreaterThan(0).WithMessage("O valor do produto vendido deve ser maior que 0!");
        }
    }
}