﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using SCGV.Domain.Interfaces.Converter;
using SCGV.Domain.Interfaces.Converter.Responses;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Domain.Interfaces.Services;
using SCGV.Infra.Data.Context;
using SCGV.Infra.Data.Repositories;
using SCGV.Service.Conversions;
using SCGV.Service.Responses.Conversions;
using SCGV.Service.Services;

namespace SCGV.Application
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration["MySqlConnection:MySqlConnectionString"];

            services.AddDbContext<SCGVContext>(options => options.UseMySql(connectionString));

            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddScoped<IArquivoService, ArquivoService>();

            services.AddScoped<IClienteService, ClienteService>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IClienteConverter, ClienteConverter>();
            services.AddScoped<IResponseClienteConverter, ResponseClienteConverter>();

            services.AddScoped<IColaboradorService, ColaboradorService>();
            services.AddScoped<IColaboradorRepository, ColaboradorRepository>();
            services.AddScoped<IColaboradorConverter, ColaboradorConverter>();
            services.AddScoped<IResponseColaboradorConverter, ResponseColaboradorConverter>();

            services.AddScoped<IFornecedorService, FornecedorService>();
            services.AddScoped<IFornecedorRepository, FornecedorRepository>();
            services.AddScoped<IFornecedorConverter, FornecedorConverter>();
            services.AddScoped<IResponseFornecedorConverter, ResponseFornecedorConverter>();

            services.AddScoped<IProdutoService, ProdutoService>();
            services.AddScoped<IProdutoRepository, ProdutoRepository>();
            services.AddScoped<IProdutoConverter, ProdutoConverter>();
            services.AddScoped<IResponseProdutoConverter, ResponseProdutoConverter>();

            services.AddScoped<IServicoService, ServicoService>();
            services.AddScoped<IServicoRepository, ServicoRepository>();
            services.AddScoped<IServicoConverter, ServicoConverter>();
            services.AddScoped<IResponseServicoConverter, ResponseServicoConverter>();

            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IVendaRepository, VendaRepository>();
            services.AddScoped<IVendaConverter, VendaConverter>();
            services.AddScoped<IResponseVendaConverter, ResponseVendaConverter>();

            services.AddScoped<IVendaProdutoService, VendaProdutoService>();
            services.AddScoped<IVendaProdutoRepository, VendaProdutoRepository>();
            services.AddScoped<IVendaProdutoConverter, VendaProdutoConverter>();
            services.AddScoped<IResponseVendaProdutoConverter, ResponseVendaProdutoConverter>();

            services.AddScoped<IVendaServicoService, VendaServicoService>();
            services.AddScoped<IVendaServicoRepository, VendaServicoRepository>();
            services.AddScoped<IVendaServicoConverter, VendaServicoConverter>();
            services.AddScoped<IResponseVendaServicoConverter, ResponseVendaServicoConverter>();

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.FormatterMappings.SetMediaTypeMappingForFormat("xml", MediaTypeHeaderValue.Parse("text/xml"));
                options.FormatterMappings.SetMediaTypeMappingForFormat("json", MediaTypeHeaderValue.Parse("application/json"));

            }).AddXmlSerializerFormatters();

            services.AddMvc().AddNewtonsoftJson(options => 
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddApiVersioning();

            services.AddSwaggerGen(s => 
            {
                s.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Sistema de Controle e Gestão de Vendas",
                    Version = "v1",
                    Description = "API de Controle e Gestão de Vendas"
                });
            });
        }

        [System.Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(s => 
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json","API SCGV V1");
            });

            var option = new RewriteOptions();

            option.AddRedirect("^$", "swagger");

            app.UseRewriter(option);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}