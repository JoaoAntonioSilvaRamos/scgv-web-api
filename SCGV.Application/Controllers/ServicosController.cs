﻿using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Services;
using SCGV.Service.Validators;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class ServicosController : ControllerBase
    {
        private readonly IServicoService _service;

        public ServicosController(IServicoService service)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(200)]
        [SwaggerResponse((201), Type = typeof(ServicoDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Post([FromBody] ServicoDTO servicoDTO)
        {
            try
            {
                _service.Post<ServicoDTOValidator>(servicoDTO);

                return Created("", servicoDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(ServicoDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Put(int id, [FromBody] ServicoDTO servicoDTO)
        {
            try
            {
                _service.Put<ServicoDTOValidator>(id, servicoDTO);

                return Ok(servicoDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPatch("{id}/{status}")]
        [SwaggerResponse((200), Type = typeof(ServicoDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Patch(int id, bool status)
        {
            try
            {
                _service.Patch(id, status);

                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpDelete]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse(200)]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);

                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(ServicoDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse((200), Type = typeof(IList<ServicoDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(_service.GetAll());
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("all")]
        [SwaggerResponse((200), Type = typeof(ICollection<ResponseServicoDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAllServicesByPagination([FromQuery]int pageIndex, [FromQuery]int pageSize, [FromQuery]string descricao, [FromQuery]decimal preco)
        {
            try
            {
                ServicoFilter servicoFilter = new ServicoFilter
                {
                    Descricao = descricao,
                    Preco = preco
                };

                return Ok(_service.GetAll(pageIndex, pageSize, servicoFilter));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}