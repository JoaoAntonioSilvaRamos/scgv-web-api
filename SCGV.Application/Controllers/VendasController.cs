﻿using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.DTOs;
using SCGV.Domain.Interfaces.Services;
using SCGV.Service.Validators;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class VendasController : ControllerBase
    {
        private readonly IVendaService _service;

        public VendasController(IVendaService service)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(200)]
        [SwaggerResponse((201), Type = typeof(VendaDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Post([FromBody] VendaDTO vendaDTO, [FromQuery]List<int> listaServicos, [FromQuery]List<int> listaProdutos)
        {
            try
            {
                _service.Post<VendaDTOValidator>(vendaDTO, listaServicos, listaProdutos);

                return Created("", vendaDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPut]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse((200), Type = typeof(VendaDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Put([FromBody] VendaDTO vendaDTO)
        {
            try
            {
                _service.Put<VendaDTOValidator>(vendaDTO);

                return Ok(vendaDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPatch("{id}/{status}")]
        [SwaggerResponse((200), Type = typeof(VendaDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Patch(int id, bool status)
        {
            try
            {
                _service.Patch(id, status);

                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpDelete]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse(200)]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);

                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(VendaDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse((200), Type = typeof(IList<VendaDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(_service.GetAll());
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("all")]
        [SwaggerResponse((200), Type = typeof(ICollection<ResponseVendaDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll([FromQuery]int pageIndex, [FromQuery]int pageSize)
        {
            try
            {
                return Ok(_service.GetAll(pageIndex, pageSize));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}