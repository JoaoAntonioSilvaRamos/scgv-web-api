﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.Interfaces.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class ArquivosController : ControllerBase
    {
        private readonly IArquivoService _service;

        public ArquivosController(IArquivoService service)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(200)]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public async Task<IActionResult> Post([FromForm] IFormFile arquivo)
        {
            try
            {
                await _service.Upload(arquivo);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}