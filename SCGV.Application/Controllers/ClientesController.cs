﻿using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Services;
using SCGV.Service.Validators;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class ClientesController : ControllerBase
    {
        private readonly IClienteService _service;

        public ClientesController(IClienteService service)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(200)]
        [SwaggerResponse((201), Type = typeof(ClienteDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Post([FromBody] ClienteDTO clienteDTO)
        {
            try
            {
                _service.Post<ClienteDTOValidator>(clienteDTO);

                return Created("", clienteDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(ClienteDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Put(int id, [FromBody] ClienteDTO clienteDTO)
        {
            try
            {
                _service.Put<ClienteDTOValidator>(id, clienteDTO);

                return Ok(clienteDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPatch("{id}/{status}")]
        [SwaggerResponse((200), Type = typeof(ClienteDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Patch(int id, bool status)
        {
            try
            {
                _service.Patch(id, status);

                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpDelete]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse(200)]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);

                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(ClienteDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse((200), Type = typeof(IList<ClienteDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(_service.GetAll());
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("all")]
        [SwaggerResponse((200), Type = typeof(ICollection<ResponseClienteDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        public IActionResult FindAll([FromQuery]int pageIndex, [FromQuery]int pageSize, [FromQuery]string nome, [FromQuery]string cpf)
        {
            try
            {
                ClienteFilter clienteFilter = new ClienteFilter
                {
                    Nome = nome,
                    CPF = cpf
                };

                return Ok(_service.GetAll(pageIndex, pageSize, clienteFilter));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}