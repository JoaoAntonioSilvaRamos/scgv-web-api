﻿using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.DTOs;
using SCGV.Domain.Interfaces.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class VendasServicosController : ControllerBase
    {
        private readonly IVendaServicoService _service;

        public VendasServicosController(IVendaServicoService service)
        {
            _service = service;
        }

        [HttpGet("all")]
        [SwaggerResponse((200), Type = typeof(ICollection<ResponseVendaServicoDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll([FromQuery]int pageIndex, [FromQuery]int pageSize)
        {
            try
            {
                return Ok(_service.GetAll(pageIndex, pageSize));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}