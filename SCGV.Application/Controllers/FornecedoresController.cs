﻿using Microsoft.AspNetCore.Mvc;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Services;
using SCGV.Service.Validators;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;

namespace SCGV.Application.Controllers
{
    [ApiVersion("1")]
    [Route("api/[controller]/v{version:apiVersion}")]
    public class FornecedoresController : ControllerBase
    {
        private readonly IFornecedorService _service;

        public FornecedoresController(IFornecedorService service)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(200)]
        [SwaggerResponse((201), Type = typeof(FornecedorDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Post([FromBody] FornecedorDTO fornecedorDTO)
        {
            try
            {
                _service.Post<FornecedorDTOValidator>(fornecedorDTO);

                return Created("", fornecedorDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPut("{id}")]
        [SwaggerResponse((200), Type = typeof(FornecedorDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Put(int id, [FromBody] FornecedorDTO fornecedorDTO)
        {
            try
            {
                _service.Put<FornecedorDTOValidator>(id, fornecedorDTO);

                return Ok(fornecedorDTO);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPatch("{id}/{status}")]
        [SwaggerResponse((200), Type = typeof(FornecedorDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Patch(int id, bool status)
        {
            try
            {
                _service.Patch(id, status);

                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpDelete]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse(200)]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);

                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerResponse((200), Type = typeof(FornecedorDTO))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindById(int id)
        {
            try
            {
                return Ok(_service.GetById(id));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerResponse((200), Type = typeof(IList<FornecedorDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll()
        {
            try
            {
                return Ok(_service.GetAll());
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("all")]
        [SwaggerResponse((200), Type = typeof(ICollection<ResponseFornecedorDTO>))]
        [SwaggerResponse(204)]
        [SwaggerResponse(400)]
        [SwaggerResponse(404)]
        public IActionResult FindAll([FromQuery] int pageIndex, [FromQuery] int pageSize, [FromQuery] string nomeFantasia, [FromQuery] string cnpj)
        {
            try
            {
                FornecedorFilter fornecedorFilter = new FornecedorFilter
                {
                    NomeFantasia = nomeFantasia,
                    CNPJ = cnpj
                };

                return Ok(_service.GetAll(pageIndex, pageSize, fornecedorFilter));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}