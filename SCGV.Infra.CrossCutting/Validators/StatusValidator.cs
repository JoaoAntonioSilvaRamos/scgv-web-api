﻿using SCGV.Infra.CrossCutting.Constants;
using System;

namespace SCGV.Infra.CrossCutting.Validators
{
    public class StatusValidator
    {
        public bool ValidateStatus(bool status)
        {
            if (status.Equals(true) || status.Equals(false))
            {
                return true;
            }
            else
            {
                throw new Exception(MensagensGerais.StatusDeveSer_Verdadeiro_Ou_Falso.GetMessage("Status"));
            }
        }
    }
}