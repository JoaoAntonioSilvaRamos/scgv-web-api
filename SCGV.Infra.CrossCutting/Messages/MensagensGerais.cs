﻿namespace SCGV.Infra.CrossCutting.Constants
{
    public class MensagensGerais
    {
        public static MensagensGerais MatriculaDeveSerMaiorQue_0 { get { return new MensagensGerais(" A {0} deve ser maior que zero!"); } }
        public static MensagensGerais MatriculaDeveSer_0 { get { return new MensagensGerais(" A {0} deve ser zero!"); } }
        public static MensagensGerais CodigoDeveSerMaiorQue_0 { get { return new MensagensGerais(" O {0} deve ser maior que zero!"); } }
        public static MensagensGerais CodigoDeveSer_0 { get { return new MensagensGerais(" O {0} deve ser zero!"); } }
        public static MensagensGerais ArquivoNaoPodeSerNuloOuVazio { get { return new MensagensGerais(" O arquivo não pode ser nulo ou vazio!"); } }
        public static MensagensGerais ObjetoNaoEncontrado { get { return new MensagensGerais(" {0} não encontrado!"); } }
        public static MensagensGerais DadosInseridosEstaoIncorretos { get { return new MensagensGerais(" Os dados inseridos estão inválidos, por favor verifique!"); } }
        public static MensagensGerais StatusDeveSer_Verdadeiro_Ou_Falso { get { return new MensagensGerais(" O {0} deve ser true ou false!"); } }

        private string Mensagem { get; set; }

        private MensagensGerais(string mensagem)
        { 
            Mensagem = mensagem; 
        }

        public string GetMessage(params object[] args)
        {
            try 
            { 
                return string.Format(Mensagem, args); 
            } 
            catch 
            { 
                return Mensagem; 
            }
        }

        public string GetMessage()
        {
            return Mensagem;
        }
    }
}