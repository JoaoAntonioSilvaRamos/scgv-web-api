﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class VendaProdutoEntityTypeConfiguration : IEntityTypeConfiguration<VendaProduto>
    {
        public void Configure(EntityTypeBuilder<VendaProduto> entity)
        {
            entity.ToTable("VendasProdutos");

            entity.HasKey(vp => new { vp.IdVenda, vp.IdProduto });

            entity.Property(vp => vp.Quantidade)
                .IsRequired()
                .HasColumnName("Quantidade")
                .HasColumnType("int");

            entity.Property(vp => vp.Valor)
                .IsRequired()
                .HasColumnName("Valor")
                .HasColumnType("decimal(9,2)");

            entity.HasOne(vp => vp.Venda)
                .WithMany(vp => vp.ListaVendasDeProdutosPorVenda)
                .HasForeignKey(vp => vp.IdVenda);

            entity.HasOne(vp => vp.Produto)
                .WithMany(vp => vp.ListaVendasDeProdutosPorProduto)
                .HasForeignKey(vp => vp.IdProduto);
        }
    }
}