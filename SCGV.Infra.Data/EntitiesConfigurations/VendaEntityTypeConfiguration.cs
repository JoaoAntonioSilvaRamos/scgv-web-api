﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class VendaEntityTypeConfiguration : IEntityTypeConfiguration<Venda>
    {
        public void Configure(EntityTypeBuilder<Venda> entity)
        {
            entity.ToTable("Vendas");

            entity.HasKey(v => v.Id);

            entity.Property(v => v.Data)
                .IsRequired()
                .HasColumnName("Data")
                .HasColumnType("datetime");

            entity.Property(v => v.Total)
                .IsRequired()
                .HasColumnName("Total")
                .HasColumnType("decimal(9,2)");

            entity.Property(v => v.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("boolean");

            entity.HasOne(v => v.Cliente)
                .WithMany(v => v.ListaVendasPorCliente)
                .HasForeignKey(v => v.IdCliente);

            entity.HasOne(v => v.Colaborador)
                .WithMany(v => v.ListaVendasPorColaborador)
                .HasForeignKey(v => v.IdColaborador);
        }
    }
}