﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class ClienteEntityTypeConfiguration : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> entity)
        {
            entity.ToTable("Clientes");

            entity.HasKey(c => c.Id);

            entity.Property(c => c.Nome)
                .IsRequired()
                .HasColumnName("Nome")
                .HasColumnType($"varchar({255})");

            entity.Property(c => c.Sexo)
                .IsRequired()
                .HasColumnName("Sexo")
                .HasColumnType($"varchar({1})");

            entity.Property(c => c.CPF)
                .IsRequired()
                .HasColumnName("CPF")
                .HasColumnType($"varchar({14})");

            entity.Property(c => c.NumeroTelefoneCelular)
                .IsRequired()
                .HasColumnName("NumeroTelefoneCelular")
                .HasColumnType($"varchar({13})");

            entity.Property(c => c.DataCadastro)
                .IsRequired()
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime");

            entity.Property(c => c.DataUltimaAlteracao)
                .IsRequired()
                .HasColumnName("DataUltimaAlteracao")
                .HasColumnType("datetime");

            entity.Property(c => c.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("boolean");
        }
    }
}