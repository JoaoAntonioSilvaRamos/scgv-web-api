﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class FornecedorEntityTypeConfiguration : IEntityTypeConfiguration<Fornecedor>
    {
        public void Configure(EntityTypeBuilder<Fornecedor> entity)
        {
            entity.ToTable("Fornecedores");

            entity.HasKey(f => f.Id);

            entity.Property(f => f.RazaoSocial)
                .IsRequired()
                .HasColumnName("RazaoSocial")
                .HasColumnType($"varchar({255})");

            entity.Property(f => f.NomeFantasia)
                .IsRequired()
                .HasColumnName("NomeFantasia")
                .HasColumnType($"varchar({255})");

            entity.Property(f => f.CNPJ)
                .IsRequired()
                .HasColumnName("CNPJ")
                .HasColumnType($"varchar({18})");

            entity.Property(f => f.NumeroTelefoneCelular)
                .IsRequired()
                .HasColumnName("NumeroTelefoneCelular")
                .HasColumnType($"varchar({13})");

            entity.Property(f => f.DataCadastro)
                .IsRequired()
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime");

            entity.Property(f => f.DataUltimaAlteracao)
                .IsRequired()
                .HasColumnName("DataUltimaAlteracao")
                .HasColumnType("datetime");

            entity.Property(f => f.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("boolean");
        }
    }
}