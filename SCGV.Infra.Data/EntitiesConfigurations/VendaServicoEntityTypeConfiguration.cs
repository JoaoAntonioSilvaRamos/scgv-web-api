﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class VendaServicoEntityTypeConfiguration : IEntityTypeConfiguration<VendaServico>
    {
        public void Configure(EntityTypeBuilder<VendaServico> entity)
        {
            entity.ToTable("VendasServicos");

            entity.HasKey(vs => new { vs.IdVenda, vs.IdServico });

            entity.Property(vs => vs.Quantidade)
                .IsRequired()
                .HasColumnName("Quantidade")
                .HasColumnType("int");

            entity.Property(vs => vs.Valor)
                .IsRequired()
                .HasColumnName("Valor")
                .HasColumnType("decimal(9,2)");

            entity.HasOne(vs => vs.Venda)
                .WithMany(vs => vs.ListaVendasDeServicosPorVenda)
                .HasForeignKey(vs => vs.IdVenda);

            entity.HasOne(vs => vs.Servico)
                .WithMany(vs => vs.ListaVendasDeServicosPorServico)
                .HasForeignKey(vs => vs.IdServico);
        }
    }
}