﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class ServicoEntityTypeConfiguration : IEntityTypeConfiguration<Servico>
    {
        public void Configure(EntityTypeBuilder<Servico> entity)
        {
            entity.ToTable("Servicos");

            entity.HasKey(s => s.Id);

            entity.Property(s => s.Descricao)
                .IsRequired()
                .HasColumnName("Descricao")
                .HasColumnType($"varchar({255})");

            entity.Property(s => s.Preco)
                .IsRequired()
                .HasColumnName("Preco")
                .HasColumnType("decimal(9,2)");

            entity.Property(s => s.DataCadastro)
                .IsRequired()
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime");

            entity.Property(s => s.DataUltimaAlteracao)
                .IsRequired()
                .HasColumnName("DataUltimaAlteracao")
                .HasColumnType("datetime");

            entity.Property(s => s.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("boolean");
        }
    }
}