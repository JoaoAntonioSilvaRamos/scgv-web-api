﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCGV.Domain.Entities;

namespace SCGV.Infra.Data.EntitiesConfigurations
{
    public class ProdutoEntityTypeConfiguration : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> entity)
        {
            entity.ToTable("Produtos");

            entity.HasKey(p => p.Id);

            entity.Property(p => p.CodigoBarras)
                .IsRequired()
                .HasColumnName("CodigoBarras")
                .HasColumnType($"varchar({255})");

            entity.Property(p => p.Descricao)
                .IsRequired()
                .HasColumnName("Descricao")
                .HasColumnType($"varchar({255})");

            entity.Property(p => p.Quantidade)
                .IsRequired()
                .HasColumnName("Quantidade")
                .HasColumnType("decimal(9,2)");

            entity.Property(p => p.UnidadeMedida)
                .IsRequired()
                .HasColumnName("UnidadeMedida")
                .HasColumnType($"varchar({5})");

            entity.Property(p => p.PrecoUnitario)
                .IsRequired()
                .HasColumnName("PrecoUnitario")
                .HasColumnType("decimal(9,2)");

            entity.Property(p => p.NotaFiscalCompra)
                .IsRequired()
                .HasColumnName("NotaFiscalCompra")
                .HasColumnType($"varchar({10})");

            entity.Property(p => p.DataCadastro)
                .IsRequired()
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime");

            entity.Property(p => p.DataUltimaAlteracao)
                .IsRequired()
                .HasColumnName("DataUltimaAlteracao")
                .HasColumnType("datetime");

            entity.Property(p => p.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("boolean");

            entity.HasOne(p => p.Fornecedor)
                .WithMany(p => p.ListaProdutosPorFornecedor)
                .HasForeignKey(p => p.IdFornecedor);
        }
    }
}