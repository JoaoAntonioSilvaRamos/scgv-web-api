﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class ServicoRepository : BaseRepository<Servico>, IServicoRepository
    {
        private readonly SCGVContext _context;

        public ServicoRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Servico> SelectAll(int pageIndex, int pageSize, ServicoFilter servicoFilter)
        {
            var query = _context.Set<Servico>()
                .Include(s => s.ListaVendasDeServicosPorServico)
                .Where(s => (s.Id >= ConstantesGerais.UM || s.Descricao == servicoFilter.Descricao || s.Preco == servicoFilter.Preco))
                .OrderBy(s => s.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }

        public DateTime SelectRegistrationDateById(int id)
        {
            var servico = SelectById(id);

            return servico.DataCadastro;
        }
    }
}