﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class VendaServicoRepository : BaseRepository<VendaServico>, IVendaServicoRepository
    {
        private readonly SCGVContext _context;

        public VendaServicoRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<VendaServico> SelectAll(int pageIndex, int pageSize)
        {
            var query = _context.Set<VendaServico>()
                .Include(vs => vs.Venda)
                    .ThenInclude(c => c.Cliente)
                .Include(vs => vs.Venda)
                    .ThenInclude(c => c.Colaborador)
                .Include(vs => vs.Servico)
                .OrderBy(vs => vs.IdVenda)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }
    }
}