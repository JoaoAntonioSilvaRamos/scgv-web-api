﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class VendaProdutoRepository : BaseRepository<VendaProduto>, IVendaProdutoRepository
    {
        private readonly SCGVContext _context;

        public VendaProdutoRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<VendaProduto> SelectAll(int pageIndex, int pageSize)
        {
            var query = _context.Set<VendaProduto>()
                .Include(vp => vp.Venda)
                    .ThenInclude(c => c.Cliente)
                .Include(vp => vp.Venda)
                    .ThenInclude(c => c.Colaborador)
                .Include(vp => vp.Produto)
                    .ThenInclude(f => f.Fornecedor)
                .OrderBy(vs => vs.IdVenda)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }
    }
}