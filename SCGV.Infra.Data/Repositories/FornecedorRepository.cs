﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class FornecedorRepository : BaseRepository<Fornecedor>, IFornecedorRepository
    {
        private readonly SCGVContext _context;

        public FornecedorRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Fornecedor> SelectAll(int pageIndex, int pageSize, FornecedorFilter fornecedorFilter)
        {
            var query = _context.Set<Fornecedor>()
                .Include(f => f.ListaProdutosPorFornecedor)
                .Where(f => (f.Id >= ConstantesGerais.UM || f.NomeFantasia == fornecedorFilter.NomeFantasia || f.CNPJ == fornecedorFilter.CNPJ))
                .OrderBy(f => f.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }

        public DateTime SelectRegistrationDateById(int id)
        {
            var fornecedor = SelectById(id);

            return fornecedor.DataCadastro;
        }
    }
}