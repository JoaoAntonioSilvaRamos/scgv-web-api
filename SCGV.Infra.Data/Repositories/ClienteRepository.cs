﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {
        private readonly SCGVContext _context;

        public ClienteRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Cliente> SelectAll(int pageIndex, int pageSize, ClienteFilter clienteFilter)
        {
            var query = _context.Set<Cliente>()
                .Include(c => c.ListaVendasPorCliente)
                .Where(c => (c.Id >= ConstantesGerais.UM || c.Nome == clienteFilter.Nome || c.CPF == clienteFilter.CPF))
                .OrderBy(c => c.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }

        public DateTime SelectRegistrationDateById(int id)
        {
            var cliente = SelectById(id);

            return cliente.DataCadastro;
        }
    }
}