﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class VendaRepository : BaseRepository<Venda>, IVendaRepository
    {
        private readonly SCGVContext _context;

        public VendaRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Venda> SelectAll(int pageIndex, int pageSize)
        {
            var query = _context.Set<Venda>()
                .Include(v => v.Cliente)
                .Include(v => v.Colaborador)
                .OrderBy(v => v.Data)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }
    }
}