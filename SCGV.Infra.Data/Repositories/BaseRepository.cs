﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Interfaces.Repositories.Base;
using SCGV.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class BaseRepository<E> : IBaseRepository<E> where E : class
    {
        private readonly SCGVContext _context;

        public BaseRepository(SCGVContext context)
        {
            _context = context;
        }

        public void Insert(E obj)
        {
            _context.Set<E>().Add(obj);

            _context.SaveChanges();
        }

        public void Update(E obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.Set<E>().Remove(SelectById(id));

            _context.SaveChanges();
        }

        public E SelectById(int id)
        {
            return _context.Set<E>().Find(id);
        }

        public IList<E> SelectAll()
        {
            return _context.Set<E>().ToList();
        }
    }
}