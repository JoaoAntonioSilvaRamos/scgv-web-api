﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class ProdutoRepository : BaseRepository<Produto>, IProdutoRepository
    {
        private readonly SCGVContext _context;

        public ProdutoRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Produto> SelectAll(int pageIndex, int pageSize, ProdutoFilter produtoFilter)
        {
            var query = _context.Set<Produto>()
                .Include(p => p.Fornecedor)
                .Include(p => p.ListaVendasDeProdutosPorProduto)
                .Where(p => (p.Id >= ConstantesGerais.UM || p.CodigoBarras == produtoFilter.CodigoBarras || p.Descricao == produtoFilter.Descricao || p.IdFornecedor == produtoFilter.IdFornecedor))
                .OrderBy(p => p.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }

        public DateTime SelectRegistrationDateById(int id)
        {
            var produto = SelectById(id);

            return produto.DataCadastro;
        }
    }
}