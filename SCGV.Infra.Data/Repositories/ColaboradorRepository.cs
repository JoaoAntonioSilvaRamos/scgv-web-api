﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories;
using SCGV.Infra.CrossCutting.Constants;
using SCGV.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCGV.Infra.Data.Repositories
{
    public class ColaboradorRepository : BaseRepository<Colaborador>, IColaboradorRepository
    {
        private readonly SCGVContext _context;

        public ColaboradorRepository(SCGVContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Colaborador> SelectAll(int pageIndex, int pageSize, ColaboradorFilter colaboradorFilter)
        {
            var query = _context.Set<Colaborador>()
                .Include(c => c.ListaVendasPorColaborador)
                .Where(c => (c.Id >= ConstantesGerais.UM || c.Nome == colaboradorFilter.Nome || c.CPF == colaboradorFilter.CPF || c.Funcao == colaboradorFilter.Funcao))
                .OrderBy(c => c.Id)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize);

            return query.ToList();
        }

        public DateTime SelectRegistrationDateById(int id)
        {
            var colaborador = SelectById(id);

            return colaborador.DataCadastro;
        }
    }
}