﻿using Microsoft.EntityFrameworkCore;
using SCGV.Domain.Entities;
using SCGV.Infra.Data.EntitiesConfigurations;

namespace SCGV.Infra.Data.Context
{
    public class SCGVContext : DbContext
    {
        public SCGVContext()
        {

        }

        public SCGVContext(DbContextOptions<SCGVContext> options) : base(options)
        {

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Colaborador> Colaboradores { get; set; }
        public DbSet<Fornecedor> Fornecedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Servico> Servicos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<VendaProduto> VendasProdutos { get; set; }
        public DbSet<VendaServico> VendasServicos { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySql("Server=localhost;Port=3306;DataBase=SCGV;Uid=root;Pwd=;SslMode=none;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Cliente>(new ClienteEntityTypeConfiguration().Configure);
            modelBuilder.Entity<Colaborador>(new ColaboradorEntityTypeConfiguration().Configure);
            modelBuilder.Entity<Fornecedor>(new FornecedorEntityTypeConfiguration().Configure);
            modelBuilder.Entity<Produto>(new ProdutoEntityTypeConfiguration().Configure);
            modelBuilder.Entity<Servico>(new ServicoEntityTypeConfiguration().Configure);
            modelBuilder.Entity<Venda>(new VendaEntityTypeConfiguration().Configure);
            modelBuilder.Entity<VendaProduto>(new VendaProdutoEntityTypeConfiguration().Configure);
            modelBuilder.Entity<VendaServico>(new VendaServicoEntityTypeConfiguration().Configure);
        }
    }
}