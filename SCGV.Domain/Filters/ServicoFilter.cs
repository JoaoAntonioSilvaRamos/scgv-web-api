﻿using SCGV.Domain.Filters.Base;

namespace SCGV.Domain.Filters
{
    public class ServicoFilter : BaseFilter
    {
        public string Descricao { get; set; }
        public decimal Preco { get; set; }
    }
}