﻿using SCGV.Domain.Filters.Base;

namespace SCGV.Domain.Filters
{
    public class ProdutoFilter : BaseFilter
    {
        public string CodigoBarras { get; set; }
        public string Descricao { get; set; }
        public int IdFornecedor { get; set; }
    }
}