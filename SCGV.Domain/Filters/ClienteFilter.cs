﻿using SCGV.Domain.Filters.Base;

namespace SCGV.Domain.Filters
{
    public class ClienteFilter : BaseFilter
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
    }
}