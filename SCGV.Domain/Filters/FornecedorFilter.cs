﻿using SCGV.Domain.Filters.Base;

namespace SCGV.Domain.Filters
{
    public class FornecedorFilter : BaseFilter
    {
        public string NomeFantasia { get; set; }
        public string CNPJ { get; set; }
    }
}