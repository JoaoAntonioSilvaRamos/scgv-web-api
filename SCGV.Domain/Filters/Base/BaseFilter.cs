﻿namespace SCGV.Domain.Filters.Base
{
    public class BaseFilter
    {
        public virtual int Id { get; set; }
    }
}