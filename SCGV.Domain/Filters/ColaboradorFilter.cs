﻿using SCGV.Domain.Filters.Base;

namespace SCGV.Domain.Filters
{
    public class ColaboradorFilter : BaseFilter
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Funcao { get; set; }
    }
}