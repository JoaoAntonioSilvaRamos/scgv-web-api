﻿namespace SCGV.Domain.Entities
{
    public class VendaProduto
    {
        public int IdVenda { get; set; }
        public virtual Venda Venda { get; set; }
        public int IdProduto { get; set; }
        public virtual Produto Produto { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}