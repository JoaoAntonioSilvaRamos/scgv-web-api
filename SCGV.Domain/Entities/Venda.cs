﻿using SCGV.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Entities
{
    public class Venda : BaseEntity
    {
        public DateTime Data { get; set; }
        public int IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
        public int IdColaborador { get; set; }
        public virtual Colaborador Colaborador { get; set; }
        public decimal Total { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<VendaProduto> ListaVendasDeProdutosPorVenda { get; set; }
        public virtual ICollection<VendaServico> ListaVendasDeServicosPorVenda { get; set; }
    }
}