﻿using SCGV.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Entities
{
    public class Produto : BaseEntity
    {
        public string CodigoBarras { get; set; }
        public string Descricao { get; set; }
        public decimal Quantidade { get; set; }
        public string UnidadeMedida { get; set; }
        public decimal PrecoUnitario { get; set; }
        public string NotaFiscalCompra { get; set; }
        public int IdFornecedor { get; set; }
        public virtual Fornecedor Fornecedor { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataUltimaAlteracao { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<VendaProduto> ListaVendasDeProdutosPorProduto { get; set; }
    }
}