﻿namespace SCGV.Domain.Entities
{
    public class VendaServico
    {
        public int IdVenda { get; set; }
        public virtual Venda Venda { get; set; }
        public int IdServico { get; set; }
        public virtual Servico Servico { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}