﻿using SCGV.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Entities
{
    public class Cliente : BaseEntity
    {
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }
        public string NumeroTelefoneCelular { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataUltimaAlteracao { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<Venda> ListaVendasPorCliente { get; set; }
    }
}