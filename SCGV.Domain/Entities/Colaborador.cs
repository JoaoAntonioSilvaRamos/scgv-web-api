﻿using SCGV.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Entities
{
    public class Colaborador : BaseEntity
    {
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }
        public string NumeroTelefoneCelular { get; set; }
        public string Funcao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataUltimaAlteracao { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<Venda> ListaVendasPorColaborador { get; set; }
    }
}