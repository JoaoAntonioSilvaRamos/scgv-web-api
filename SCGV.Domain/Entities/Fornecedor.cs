﻿using SCGV.Domain.Entities.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Entities
{
    public class Fornecedor : BaseEntity
    {
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJ { get; set; }
        public string NumeroTelefoneCelular { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataUltimaAlteracao { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<Produto> ListaProdutosPorFornecedor { get; set; }
    }
}