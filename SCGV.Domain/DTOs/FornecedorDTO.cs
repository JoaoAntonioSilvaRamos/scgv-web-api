﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class FornecedorDTO
    {
        [DataMember(Name = "Matrícula do Fornecedor")]
        public int Id { get; set; }

        [DataMember(Name = "Razão Social")]
        public string RazaoSocial { get; set; }

        [DataMember(Name = "Nome Fantasia")]
        public string NomeFantasia { get; set; }

        [DataMember(Name = "CNPJ")]
        public string CNPJ { get; set; }

        [DataMember(Name = "Número do Celular")]
        public string NumeroTelefoneCelular { get; set; }

        [DataMember(Name = "Ativo")]
        public bool Status { get; set; }
    }

    public class ResponseFornecedorDTO : FornecedorDTO
    {
        [DataMember(Order = 1, Name = "Lista de Produtos do Fornecedor")]
        public virtual ICollection<ProdutoDTO> ListaProdutosPorFornecedor { get; set; }
    }
}