﻿using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class VendaServicoDTO
    {
        [DataMember(Name = "Número da Venda")]
        public int IdVenda { get; set; }

        [DataMember(Name = "Código do Serviço")]
        public int IdServico { get; set; }

        [DataMember(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [DataMember(Name = "Valor")]
        public decimal Valor { get; set; }
    }

    public class ResponseVendaServicoDTO : VendaServicoDTO
    {
        [DataMember(Order = 1, Name = "Venda")]
        public virtual ResponseVendaDTO ResponseVendaDTO { get; set; }

        [DataMember(Order = 2, Name = "Serviço")]
        public virtual ResponseServicoDTO ResponseServicoDTO { get; set; }
    }
}