﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class ServicoDTO
    {
        [DataMember(Name = "Código do Serviço")]
        public int Id { get; set; }

        [DataMember(Name = "Descrição")]
        public string Descricao { get; set; }

        [DataMember(Name = "Preço")]
        public decimal Preco { get; set; }

        [DataMember(Name = "Ativo")]
        public bool Status { get; set; }
    }

    public class ResponseServicoDTO : ServicoDTO
    {
        [DataMember(Order = 1, Name = "Lista de Vendas de Serviço do Serviço")]
        public virtual ICollection<VendaServicoDTO> ListaVendasDeServicosPorServico { get; set; }
    }
}