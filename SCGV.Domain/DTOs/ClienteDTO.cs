﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class ClienteDTO
    {
        [DataMember(Name = "Matrícula do Cliente")]
        public int Id { get; set; }

        [DataMember(Name = "Nome")]
        public string Nome { get; set; }

        [DataMember(Name = "Sexo")]
        public string Sexo { get; set; }

        [DataMember(Name = "CPF")]
        public string CPF { get; set; }

        [DataMember(Name = "Número do Celular")]
        public string NumeroTelefoneCelular { get; set; }

        [DataMember(Name = "Ativo")]
        public bool Status { get; set; }
    }

    public class ResponseClienteDTO : ClienteDTO
    {
        [DataMember(Order = 1, Name = "Lista de Vendas do Cliente")]
        public virtual ICollection<VendaDTO> ListaVendasPorCliente { get; set; }
    }
}