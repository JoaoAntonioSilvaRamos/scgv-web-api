﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class ProdutoDTO
    {
        [DataMember(Name = "Código do Produto")]
        public int Id { get; set; }

        [DataMember(Name = "Código de Barras")]
        public string CodigoBarras { get; set; }

        [DataMember(Name = "Descrição")]
        public string Descricao { get; set; }

        [DataMember(Name = "Quantidade")]
        public decimal Quantidade { get; set; }

        [DataMember(Name = "Unidade de Medida")]
        public string UnidadeMedida { get; set; }

        [DataMember(Name = "Preço Unitário")]
        public decimal PrecoUnitario { get; set; }

        [DataMember(Name = "Nota Fiscal de Compra")]
        public string NotaFiscalCompra { get; set; }

        [DataMember(Name = "Matrícula do Fornecedor")]
        public int IdFornecedor { get; set; }

        [DataMember(Name = "Ativo")]
        public bool Status { get; set; }
    }

    public class ResponseProdutoDTO : ProdutoDTO
    {
        [DataMember(Order = 1, Name = "Fornecedor")]
        public virtual FornecedorDTO Fornecedor { get; set; }
        [DataMember(Order = 2, Name = "Lista de Vendas de Produto do Produto")]
        public virtual ICollection<VendaProdutoDTO> ListaVendasDeProdutosPorProduto { get; set; }
    }
}