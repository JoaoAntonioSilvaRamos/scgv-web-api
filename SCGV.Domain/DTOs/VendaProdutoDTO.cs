﻿using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class VendaProdutoDTO
    {
        [DataMember(Name = "Número da Venda")]
        public int IdVenda { get; set; }

        [DataMember(Name = "Código do Produto")]
        public int IdProduto { get; set; }

        [DataMember(Name = "Quantidade")]
        public int Quantidade { get; set; }

        [DataMember(Name = "Valor")]
        public decimal Valor { get; set; }
    }

    public class ResponseVendaProdutoDTO : VendaProdutoDTO
    {
        [DataMember(Order = 1, Name = "Venda")]
        public virtual ResponseVendaDTO ResponseVendaDTO { get; set; }

        [DataMember(Order = 2, Name = "Produto")]
        public virtual ResponseProdutoDTO ResponseProdutoDTO { get; set; }
    }
}