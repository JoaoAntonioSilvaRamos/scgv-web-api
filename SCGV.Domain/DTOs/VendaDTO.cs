﻿using System;
using System.Runtime.Serialization;

namespace SCGV.Domain.DTOs
{
    [DataContract]
    public class VendaDTO
    {
        [DataMember(Name = "Número da Venda")]
        public int Id { get; set; }

        [DataMember(Name = "Data")]
        public DateTime Data { get; set; }

        [DataMember(Name = "Matrícula do Cliente")]
        public int IdCliente { get; set; }

        [DataMember(Name = "Matrícula do Colaborador")]
        public int IdColaborador { get; set; }

        [DataMember(Name = "Total")]
        public decimal Total { get; set; }

        [DataMember(Name = "Ativo")]
        public bool Status { get; set; }
    }

    public class ResponseVendaDTO : VendaDTO
    {
        [DataMember(Order = 1, Name = "Cliente")]
        public virtual ClienteDTO ClienteDTO { get; set; }
        [DataMember(Order = 2, Name = "Colaborador")]
        public virtual ColaboradorDTO ColaboradorDTO { get; set; }
    }
}