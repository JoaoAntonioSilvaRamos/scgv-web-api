﻿using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IProdutoRepository : IBaseRepository<Produto>
    {
        ICollection<Produto> SelectAll(int pageIndex, int pageSize, ProdutoFilter produtoFilter);
        DateTime SelectRegistrationDateById(int id);
    }
}