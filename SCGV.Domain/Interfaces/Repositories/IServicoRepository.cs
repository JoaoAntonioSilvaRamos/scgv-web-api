﻿using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IServicoRepository : IBaseRepository<Servico>
    {
        ICollection<Servico> SelectAll(int pageIndex, int pageSize, ServicoFilter servicoFilter);
        DateTime SelectRegistrationDateById(int id);
    }
}