﻿using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IFornecedorRepository : IBaseRepository<Fornecedor>
    {
        ICollection<Fornecedor> SelectAll(int pageIndex, int pageSize, FornecedorFilter fornecedorFilter);
        DateTime SelectRegistrationDateById(int id);
    }
}