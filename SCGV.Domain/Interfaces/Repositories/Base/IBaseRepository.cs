﻿using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories.Base
{
    public interface IBaseRepository<E> where E : class
    {
        void Insert(E obj);
        void Update(E obj);
        void Delete(int id);
        E SelectById(int id);
        IList<E> SelectAll();
    }
}