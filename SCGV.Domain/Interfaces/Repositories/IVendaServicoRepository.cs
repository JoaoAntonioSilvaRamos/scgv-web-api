﻿using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IVendaServicoRepository : IBaseRepository<VendaServico>
    {
        ICollection<VendaServico> SelectAll(int pageIndex, int pageSize);
    }
}