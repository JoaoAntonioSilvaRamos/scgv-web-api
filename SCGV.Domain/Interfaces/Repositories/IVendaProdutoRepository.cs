﻿using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IVendaProdutoRepository : IBaseRepository<VendaProduto>
    {
        ICollection<VendaProduto> SelectAll(int pageIndex, int pageSize);
    }
}