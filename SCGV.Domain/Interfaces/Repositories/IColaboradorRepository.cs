﻿using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IColaboradorRepository : IBaseRepository<Colaborador>
    {
        ICollection<Colaborador> SelectAll(int pageIndex, int pageSize, ColaboradorFilter colaboradorFilter);
        DateTime SelectRegistrationDateById(int id);
    }
}