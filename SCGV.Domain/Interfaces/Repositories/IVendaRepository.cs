﻿using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IVendaRepository : IBaseRepository<Venda>
    {
        ICollection<Venda> SelectAll(int pageIndex, int pageSize);
    }
}