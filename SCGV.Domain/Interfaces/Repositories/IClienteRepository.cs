﻿using SCGV.Domain.Entities;
using SCGV.Domain.Filters;
using SCGV.Domain.Interfaces.Repositories.Base;
using System;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Repositories
{
    public interface IClienteRepository : IBaseRepository<Cliente>
    {
        ICollection<Cliente> SelectAll(int pageIndex, int pageSize, ClienteFilter clienteFilter);
        DateTime SelectRegistrationDateById(int id);
    }
}