﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IFornecedorService
    {
        void Post<FornecedorDTOValidator>(FornecedorDTO fornecedorDTO) where FornecedorDTOValidator : AbstractValidator<FornecedorDTO>;
        void Put<FornecedorDTOValidator>(int id, FornecedorDTO fornecedorDTO) where FornecedorDTOValidator : AbstractValidator<FornecedorDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        FornecedorDTO GetById(int id);
        IList<FornecedorDTO> GetAll();
        ICollection<ResponseFornecedorDTO> GetAll(int pageIndex, int pageSize, FornecedorFilter fornecedorFilter);
    }
}