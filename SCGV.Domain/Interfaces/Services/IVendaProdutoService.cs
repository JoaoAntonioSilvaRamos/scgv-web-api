﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IVendaProdutoService
    {
        void Post<VendaProdutoDTOValidator>(VendaProdutoDTO vendaProdutoDTO) where VendaProdutoDTOValidator : AbstractValidator<VendaProdutoDTO>;
        ICollection<ResponseVendaProdutoDTO> GetAll(int pageIndex, int pageSize);
    }
}