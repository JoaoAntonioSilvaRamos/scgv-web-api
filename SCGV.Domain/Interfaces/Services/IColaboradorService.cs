﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IColaboradorService
    {
        void Post<ColaboradorDTOValidator>(ColaboradorDTO colaboradorDTO) where ColaboradorDTOValidator : AbstractValidator<ColaboradorDTO>;
        void Put<ColaboradorDTOValidator>(int id, ColaboradorDTO colaboradorDTO) where ColaboradorDTOValidator : AbstractValidator<ColaboradorDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        ColaboradorDTO GetById(int id);
        IList<ColaboradorDTO> GetAll();
        ICollection<ResponseColaboradorDTO> GetAll(int pageIndex, int pageSize, ColaboradorFilter colaboradorFilter);
    }
}