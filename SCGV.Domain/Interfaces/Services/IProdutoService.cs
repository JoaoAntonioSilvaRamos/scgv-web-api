﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IProdutoService
    {
        void Post<ProdutoDTOValidator>(ProdutoDTO produtoDTO) where ProdutoDTOValidator : AbstractValidator<ProdutoDTO>;
        void Put<ProdutoDTOValidator>(int id, ProdutoDTO produtoDTO) where ProdutoDTOValidator : AbstractValidator<ProdutoDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        ProdutoDTO GetById(int id);
        IList<ProdutoDTO> GetAll();
        ICollection<ResponseProdutoDTO> GetAll(int pageIndex, int pageSize, ProdutoFilter produtoFilter);
    }
}