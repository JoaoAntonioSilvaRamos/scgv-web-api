﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IVendaServicoService
    {
        void Post<VendaServicoDTOValidator>(VendaServicoDTO vendaServicoDTO) where VendaServicoDTOValidator : AbstractValidator<VendaServicoDTO>;
        ICollection<ResponseVendaServicoDTO> GetAll(int pageIndex, int pageSize);
    }
}