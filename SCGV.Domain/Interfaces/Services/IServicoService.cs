﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IServicoService
    {
        void Post<ServicoDTOValidator>(ServicoDTO servicoDTO) where ServicoDTOValidator : AbstractValidator<ServicoDTO>;
        void Put<ServicoDTOValidator>(int id, ServicoDTO servicoDTO) where ServicoDTOValidator : AbstractValidator<ServicoDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        ServicoDTO GetById(int id);
        IList<ServicoDTO> GetAll();
        ICollection<ResponseServicoDTO> GetAll(int pageIndex, int pageSize, ServicoFilter servicoFilter);
    }
}