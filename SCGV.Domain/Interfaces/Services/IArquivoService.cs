﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IArquivoService
    {
        Task<IFormFile> Upload(IFormFile arquivo);
    }
}