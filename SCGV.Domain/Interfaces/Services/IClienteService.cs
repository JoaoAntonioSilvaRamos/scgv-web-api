﻿using FluentValidation;
using SCGV.Domain.DTOs;
using SCGV.Domain.Filters;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IClienteService
    {
        void Post<ClienteDTOValidator>(ClienteDTO clienteDTO) where ClienteDTOValidator : AbstractValidator<ClienteDTO>;
        void Put<ClienteDTOValidator>(int id, ClienteDTO clienteDTO) where ClienteDTOValidator : AbstractValidator<ClienteDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        ClienteDTO GetById(int id);
        IList<ClienteDTO> GetAll();
        ICollection<ResponseClienteDTO> GetAll(int pageIndex, int pageSize, ClienteFilter clienteFilter);
    }
}