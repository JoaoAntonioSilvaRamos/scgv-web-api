﻿using FluentValidation;
using SCGV.Domain.DTOs;
using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Services
{
    public interface IVendaService
    {
        void Post<VendaDTOValidator>(VendaDTO vendaDTO, List<int> listaServicos, List<int> listaProdutos) where VendaDTOValidator : AbstractValidator<VendaDTO>;
        void Put<VendaDTOValidator>(VendaDTO vendaDTO) where VendaDTOValidator : AbstractValidator<VendaDTO>;
        void Patch(int id, bool status);
        void Delete(int id);
        VendaDTO GetById(int id);
        IList<VendaDTO> GetAll();
        ICollection<ResponseVendaDTO> GetAll(int pageIndex, int pageSize);
    }
}