﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter
{
    public interface IServicoConverter : IBaseConverter<ServicoDTO, Servico>, IBaseConverter<Servico, ServicoDTO> { }
}