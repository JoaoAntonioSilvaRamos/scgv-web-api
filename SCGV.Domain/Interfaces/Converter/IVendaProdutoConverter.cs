﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter
{
    public interface IVendaProdutoConverter : IBaseConverter<VendaProdutoDTO, VendaProduto>, IBaseConverter<VendaProduto, VendaProdutoDTO> { }
}