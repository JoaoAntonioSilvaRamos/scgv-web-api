﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter.Responses
{
    public interface IResponseVendaProdutoConverter : IBaseConverter<ResponseVendaProdutoDTO, VendaProduto>, IBaseConverter<VendaProduto, ResponseVendaProdutoDTO> { }
}