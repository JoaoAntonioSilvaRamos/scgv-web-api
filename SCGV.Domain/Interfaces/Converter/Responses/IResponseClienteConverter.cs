﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter.Responses
{
    public interface IResponseClienteConverter : IBaseConverter<ResponseClienteDTO, Cliente>, IBaseConverter<Cliente, ResponseClienteDTO> { }
}