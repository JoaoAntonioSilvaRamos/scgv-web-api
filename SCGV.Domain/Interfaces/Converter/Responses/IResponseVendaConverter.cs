﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter.Responses
{
    public interface IResponseVendaConverter : IBaseConverter<ResponseVendaDTO, Venda>, IBaseConverter<Venda, ResponseVendaDTO> { }
}