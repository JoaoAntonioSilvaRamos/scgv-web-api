﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter.Responses
{
    public interface IResponseProdutoConverter : IBaseConverter<ResponseProdutoDTO, Produto>, IBaseConverter<Produto, ResponseProdutoDTO> { }
}