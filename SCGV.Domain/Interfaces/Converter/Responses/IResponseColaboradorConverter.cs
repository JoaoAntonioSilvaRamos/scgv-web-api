﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter.Responses
{
    public interface IResponseColaboradorConverter : IBaseConverter<ResponseColaboradorDTO, Colaborador>, IBaseConverter<Colaborador, ResponseColaboradorDTO> { }
}