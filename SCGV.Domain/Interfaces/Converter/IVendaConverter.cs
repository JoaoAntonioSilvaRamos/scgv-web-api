﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter
{
    public interface IVendaConverter : IBaseConverter<VendaDTO, Venda>, IBaseConverter<Venda, VendaDTO> { }
}