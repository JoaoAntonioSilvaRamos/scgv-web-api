﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter
{
    public interface IColaboradorConverter : IBaseConverter<ColaboradorDTO, Colaborador>, IBaseConverter<Colaborador, ColaboradorDTO> { }
}