﻿using System.Collections.Generic;

namespace SCGV.Domain.Interfaces.Converter.Base
{
    public interface IBaseConverter<Origin, Destiny>
    {
        Destiny Conversion(Origin origin);
        IList<Destiny> ListConversion(IList<Origin> origin);
        ICollection<Destiny> CollectionConversion(ICollection<Origin> origin);
    }
}