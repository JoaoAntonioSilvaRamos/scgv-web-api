﻿using SCGV.Domain.DTOs;
using SCGV.Domain.Entities;
using SCGV.Domain.Interfaces.Converter.Base;

namespace SCGV.Domain.Interfaces.Converter
{
    public interface IVendaServicoConverter : IBaseConverter<VendaServicoDTO, VendaServico>, IBaseConverter<VendaServico, VendaServicoDTO> { }
}